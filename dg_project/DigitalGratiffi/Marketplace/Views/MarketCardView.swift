//
//  MarketCardView.swift
//  DigitalGratiffi
//
//  Created by Harvey Ho on 2022-11-21.
//

import SwiftUI

struct MarketCardView: View{
    var marketcard: MarketCard
    var width: CGFloat
    
    var body: some View{
        GeometryReader{proxy in
            //let size = proxy.size
            AsyncImage(url: URL(string: marketcard.image)) { image in
                image
                    .resizable()
                    .scaledToFill()
                    .aspectRatio(contentMode: .fill)
                    .frame(width: width-10, height:250)
                    .aspectRatio(contentMode: .fill)
                    .cornerRadius(15)
            } placeholder: {
                ZStack{
                    RoundedRectangle(cornerRadius: 15)
                        .stroke(Color.white, style: StrokeStyle(lineWidth: 2, dash: [5]))
                        .frame(height: 250)
                        
                    //Image(systemName: "photo").foregroundColor(Color.white)
                    ProgressView()
                }
            }
//            VStack {
//                if(marketcard.image != ""){
//
//                }
//                else{
//                    Image("art1")
//                        .resizable()
//                        .aspectRatio(contentMode: .fit)
//                }
//            }//VStack
//            .cornerRadius(10)
//            .overlay(
//                RoundedRectangle(cornerRadius: 10)
//                    .stroke(Color(.sRGB, red: 150/255, green: 150/255, blue: 150/255, opacity: 0.1), lineWidth: 1)
//            )
//            .padding([.top, .horizontal])
        }

    }
    
}
