//
//  Confirmation.swift
//  DigitalGratiffi
//
//  Created by Harvey Ho on 2022-10-13.
//

import SwiftUI


struct Confirmation: View {
    var bgColor: Color = .blue
    
    var body: some View {
        Text("Payment Completed!")
    }
}

