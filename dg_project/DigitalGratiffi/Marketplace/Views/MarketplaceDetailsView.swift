//
//  MarketplaceDetailsView.swift
//  DigitalGratiffi
//
//  Created by Harvey Ho on 2021-10-30.
//

import SwiftUI
import Firebase


struct MarketplaceDetailsView: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @State var isAnon: Bool
    @State var description: String = ""
    @State var price: String = ""
    @State var name: String = ""
    @State var artist: String = ""
    @State var galleryShow: Bool = false
    @State var marketplaceRevealed: Bool = false
    @State var isDisable: Bool = false
    @State var bgColor: Color = .blue
    @State var btnOpacity: Double = 1.0
    @State private var isSubmitConfirm: Bool = false
    @StateObject var cart:Cart
    @State var marketcard: MarketCard
    
    
    var body: some View {
        GeometryReader{proxy in
            let size = proxy.size
            VStack(alignment: .center) {
                if(isAnon){
                    if(marketcard.image != ""){
                        HStack(alignment: .center){
                            Spacer()
                            AsyncImage(url: URL(string: marketcard.image)) { image in
                                image.resizable()
                                    .scaledToFill()
                                    .aspectRatio(contentMode: .fill)
                                    .frame(width: size.width-20, height: 250)
                                    .cornerRadius(15)
                            } placeholder: {
                                ProgressView().frame(width: size.width-10, height: 250)
                            }
                            Spacer()
                        }

                    }

                    ScrollView{
                        HStack {
                            VStack(alignment: .leading) {
                                List {
                                    Section(header: Text(marketcard.heading)) {
                                        HStack{
                                            NavigationLink(destination: PortfolioPage()){
                                                Button(action: {
                                                    UserDefaults.standard.set(marketcard.artistID, forKey: "userId")
                                                }, label: {
                                                    Text(marketcard.artist).foregroundColor(.white)
                                                })
                                                
                                            }
                                        }


                                        HStack{
                                            Text("Price")
                                            Spacer()
                                            Text("$" + marketcard.price)
                                        }
                                        
                                        HStack{
                                            NavigationLink(destination: ARContentView(marketcard: marketcard)){
                                                Text("View in AR")
                                            }
                                        }
                                        HStack{
                                            NavigationLink(destination: AGContentView(marketcard: marketcard)){
                                                Text("ARt Gallery")
                                            }
                                        }
                                    }
                                    .headerProminence(.increased)
                                    
                                    Section(header: Text("Description")){
                                        Text(marketcard.description)
                                    }
                                }.frame(width: size.width, height: size.height/2)
                                
                            }//VStack
                            .layoutPriority(100)
                        }//HStack
                        //.padding()
                    }
                    Button(action: {
                        addToCart(addItem: marketcard)
                        isDisable.toggle()
                        bgColor = .gray
                        btnOpacity = 0.3
                        presentationMode.wrappedValue.dismiss()
                    },label: {
                        Text("Add to Cart")
                            .frame(width: 300 ,height:60)
                            .background(.black)
                            .foregroundColor(.white)
                            .font(.system(size: 17, weight: .bold, design: .default))
                            .overlay(
                                RoundedRectangle(cornerRadius: 15)
                                    .stroke(Color.white, lineWidth: 1)
                            )
                    })
                     .disabled(isDisable)
                }
                else{
                    if(marketcard.image != ""){
                        ButtonImagePopular(urlToImage: marketcard.image)
                    }
                    else{
                        Image("art1")
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                    }
                    ScrollView{
                        HStack {
                            VStack(alignment: .leading) {
                                Text("Art's Name:")
                                    .font(.system(size: 18, weight: .light, design: .rounded))
                                    .foregroundColor(.primary)
                                TextField(marketcard.heading, text:$name)
                                    .font(.system(size: 18, weight: .light, design: .rounded))
                                Text("Artist:")
                                    .font(.system(size: 18, weight: .light, design: .rounded))
                                    .foregroundColor(.primary)
                                TextField(marketcard.artist, text:$artist)
                                    .font(.system(size: 18, weight: .light, design: .rounded))
                                HStack{
                                    Text("Price: $")
                                        .font(.system(size: 18, weight: .light, design: .rounded))
                                        .foregroundColor(.primary)
                                    TextField(marketcard.price, text:$price)
                                        .font(.system(size: 18, weight: .light, design: .rounded))
                                }
                                Text("Description: ")
                                    .font(.system(size: 18, weight: .light, design: .rounded))
                                    .foregroundColor(.primary)
                                TextField(marketcard.description, text:$description)
                                    .font(.system(size: 18, weight: .light, design: .rounded))
                                Text("Sell on Marketplace?")
                                    .font(.system(size: 18, weight: .light, design: .rounded))
                                    .foregroundColor(.primary)
                                Group{
                                    Picker(selection:$marketcard.marketplaceRevealed,label:
                                        Text("Sell on Marketplace")){
                                            Text("Yes").tag(true)
                                            Text("No").tag(false)
                                        }.pickerStyle(SegmentedPickerStyle())
                                    Text("Show on Gallery?")
                                        .font(.system(size: 18, weight: .light, design: .rounded))
                                        .foregroundColor(.primary)
                                    Picker(selection:$marketcard.galleryShow,label:
                                        Text("Show on Gallery")){
                                            Text("Yes").tag(true)
                                            Text("No").tag(false)
                                        }.pickerStyle(SegmentedPickerStyle())
                                }
                            }//VStack
                            .layoutPriority(100)
                        }//HStack
                        .padding()
                    }
                    Button(action: {
                        isSubmitConfirm = true
                    }){
                        Text("Submit Changes")
                            .frame(width: 200 , height: 50, alignment: .center)
                    }
                    .background(Color.blue)
                    .foregroundColor(Color.white)
                    .cornerRadius(5)
                    .confirmationDialog("Confirm all changes?", isPresented: $isSubmitConfirm){
                        Button("Confirm"){
                            submitChanges(artid: marketcard.id)
                        }
                    }
                }
            }//VStack
        }

    }
    
    func fillBlanks(){
        if (artist.isEmpty){
            artist = marketcard.artist
        }
        if (description.isEmpty){
            description = marketcard.description
        }
        if (name.isEmpty){
            name = marketcard.heading
        }
        if (price.isEmpty){
            price = marketcard.price
        }
    }
    
    func submitChanges(artid: String){
        fillBlanks()
        let db = Firestore.firestore()
        let docRef = db.collection("arts").document(artid)
        docRef.setData(["artist":artist,
                        "description":description,
                        "image":marketcard.image,
                        "name":name,
                        "price":price,
                        "galleryShow":galleryShow,
                        "marketPlaceRevealed":marketplaceRevealed,
                        "artistID":marketcard.artistID]) { error in
            if let error = error {
                print("Error updating art: \(error)")
            } else {
                print("art successfully updated!")
            }
        }
    }
    func addToCart(addItem:MarketCard){
        cart.cartList.append(addItem)
    }
}


