//
//  CartView.swift
//  DigitalGratiffi
//
//  Created by Harvey Ho on 2021-11-07.
//

import SwiftUI
import Stripe

struct CartView: View {
    @StateObject var cart:Cart
    @State private var isActive: Bool = false
    
    private func startCheckout(completion: @escaping(String?)->Void){
        let url = URL(string: "https://abyssinian-nickel-rubidium.glitch.me/create-payment-intent")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = try? JSONEncoder().encode(cart.cartList)
        
        URLSession.shared.dataTask(with: request){ data, response, error in
            guard let data = data, error == nil,
                  (response as? HTTPURLResponse)?.statusCode == 200
            else{
                completion(nil)
                return
            }
            
            let checkoutIntentResponse = try? JSONDecoder().decode(CheckoutIntentResponse.self, from: data)
            completion(checkoutIntentResponse?.clientSecret)
            
        }.resume()
    }
    
    var body: some View {
        List{
            ForEach(cart.cartList, id: \.heading){ item in
                Text(item.heading)
            }.onDelete(perform: deleteItem)
        }
        NavigationLink(isActive: $isActive){
            CheckoutView(cart: cart)
        } label:{
            Button("Checkout"){
                startCheckout{clientSecret in
                    
                    PaymentConfig.shared.paymentIntentClientSecret = clientSecret
                    DispatchQueue.main.async {
                        isActive = true
                    }
                }
            }
        }
        
    }
    private func deleteItem(at offsets: IndexSet) {
        cart.cartList.remove(atOffsets: offsets)
    }
}

