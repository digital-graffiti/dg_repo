//
//  FavoritesView.swift
//  DigitalGratiffi
//
//  Created by Harvey Ho on 2022-11-18.
//

import SwiftUI

struct FavoritesView: View {
    @StateObject var favorite: Favorite
    @StateObject var cart: Cart
    @State var isAnon: Bool
    
    var body: some View {
        GeometryReader{proxy in
            let size = proxy.size
            ScrollView{
                ForEach(favorite.favoriteList, id: \.id){ mc in
                    NavigationLink(destination: MarketplaceDetailsView(isAnon: isAnon, cart: cart, marketcard: mc), label: {
                        MarketCardView(marketcard: mc, width: size.width)
                    }).frame(height:250)
                }//ForEach
            }
            .frame(height: 600)
        }
    }
}

