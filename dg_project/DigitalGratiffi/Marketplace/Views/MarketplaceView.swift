//
//  MarketplaceView.swift
//  DigitalGratiffi
//
//  Created by Harvey Ho on 2021-10-28.
//

import SwiftUI
import Firebase

struct FavoriteView: View{
    var marketcard: MarketCard
    @StateObject var favorite: Favorite
    @State var isFavorite: Bool = false
    
    var body: some View{
        if(isFavorite){
            Button(action:{favoriteStatus()}){
                Image(systemName: "heart.fill").foregroundColor(Color.white).font(.system(size: 22))
            }
        }else{
            Button(action:{favoriteStatus()}){
                Image(systemName:"heart").foregroundColor(Color.white).font(.system(size: 22))
            }
        }
    }
    func favoriteStatus(){
        isFavorite.toggle()
        if(isFavorite){
            favorite.favoriteList.append(marketcard)
            print(favorite.favoriteList)
        }
        else{
            if let index = favorite.favoriteList.firstIndex(of: marketcard){
                favorite.favoriteList.remove(at: index)
                print(favorite.favoriteList)
            }
        }
    }
}

struct MarketplaceView: View {
    @State private var queryFor: String
    @State private var queryValue: String
    @State private var userId: String
    @State private var isAnon: Bool
    @State private var refresh: Bool = false
    @StateObject var cart = Cart()
    @StateObject var favorite = Favorite()
    @ObservedObject var queryObject = QueryObject()
    @ObservedObject var queryArt = QueryArt()
    
    let columns = Array(repeating: GridItem(.flexible(), spacing: 10), count: 1)
    
    var body: some View {
        GeometryReader{proxy in
            let size = proxy.size
            NavigationView{
                
                VStack(alignment: .center){
                    
                    HStack(alignment: .center){
                        if(isAnon){
                            NavigationLink(destination: SearchView(userId: userId, isAnon: isAnon)){
                                Image(systemName: "magnifyingglass").foregroundColor(Color.white)
                            }
                            .buttonStyle(.bordered)
                        }
                        Button(action: {update()}){
                            Image(systemName: "arrow.clockwise").foregroundColor(Color.white)
                        }
                        .buttonStyle(.bordered)
                        if(isAnon){
                            NavigationLink(destination: CartView(cart: cart)){
                                Image(systemName: "cart").foregroundColor(Color.white)
                            }
                            .buttonStyle(.bordered)
                        }
                        if(isAnon){
                            NavigationLink(destination: FavoritesView(favorite: favorite, cart: cart, isAnon: isAnon)){
                                Image(systemName: "heart.fill").foregroundColor(Color.white)
                            }
                            .buttonStyle(.bordered)
                        }
                        NavigationLink(destination: MainView().navigationBarBackButtonHidden(true)){
                            Image(systemName: "door.right.hand.open").foregroundColor(Color.white)
                        }
                        .simultaneousGesture(TapGesture().onEnded{ logout() })
                        .buttonStyle(.bordered)
                    }
                    //end HStack
                    ScrollView(showsIndicators: false){
                        LazyVGrid(columns: columns, spacing: 20) {
                            ForEach(queryObject.artsCards, id: \.id){ mc in
                                VStack(){
                                    NavigationLink(destination: MarketplaceDetailsView(isAnon: isAnon, cart: cart, marketcard: mc), label: {
                                        MarketCardView(marketcard: mc, width: size.width)
                                            .frame(height:250)
                                    }).frame(height:250)
                                    Group{
                                        Spacer()
                                        HStack(){
                                            Text(mc.heading)
                                                .fontWeight(.bold)
                                            Spacer()
                                            if(isAnon){
                                                FavoriteView(marketcard: mc, favorite: favorite)
                                                
                                            }
                                        }.padding(.horizontal, 40)
                                            .padding(.bottom, 40)
                                        
                                    }
                                }
                                .cornerRadius(15)
                                .overlay(
                                    RoundedRectangle(cornerRadius: 10.0)
                                        .stroke(Color.white, lineWidth: 2.0)
                                )
                                .frame(width: size.width-10, height: 350)
                                //.background(Color.blue)
                                
                                
                                //end VStack
                            }// end ForEach
                        }
                    }
                } //end VStack
            }.navigationViewStyle(.stack)//end NavigationView
        }
        .onAppear{queryObject.getAllArtsForQuery(patronQueryFor: queryFor,
                                                 patronQueryValue: queryValue,
                                                 submittedUserId: userId,
                                                 anon: isAnon)}
    }// end body View
    
    func update() {
        if(isAnon == false){
            queryFor = "artistID"
            queryValue = userId
            queryObject.getAllArtsForQuery(patronQueryFor: queryFor, patronQueryValue: queryValue, submittedUserId: userId, anon: isAnon)
        } else{
            queryFor = "marketPlaceRevealed"
            queryValue = "true"
            queryObject.getAllArtsForQuery(patronQueryFor: queryFor, patronQueryValue: queryValue, submittedUserId: userId, anon: isAnon)
        }
        refresh.toggle()
    }
    
    func logout(){
        do {
            try Auth.auth().signOut()
            userId = ""
            isAnon = false
        }catch {
            print("already logged out")
            
        }
    }
    
    init(qf: String, qv: String, uid: String, a: Bool){
        _queryFor = State(initialValue: qf)
        _queryValue = State(initialValue: qv)
        _userId = State(initialValue: uid)
        _isAnon = State(initialValue: a)
        queryArt.getArtGallery()
        
        //        queryObject.getAllArtsForQuery(patronQueryFor: queryFor,
        //                                  patronQueryValue: queryValue,
        //                                  submittedUserId: userId,
        //                                  anon: isAnon)
    }
    
}//MarketplaceView


