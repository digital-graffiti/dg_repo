//
//  CheckoutView.swift
//  DigitalGratiffi
//
//  Created by Harvey Ho on 2022-10-13.
//

import SwiftUI
import Stripe
import Firebase
import SwiftSMTP

struct CheckoutView: View {
    
    @StateObject var cart:Cart
    @EnvironmentObject var profile: Profile
    @State private var message: String = ""
    @State private var isSuccess: Bool = false
    @State private var paymentMethodParams: STPPaymentMethodParams?
    let paymentGatewayController = PaymentGatewayController()
    
    private func pay(){
        
        guard let clientSecret =
                PaymentConfig.shared.paymentIntentClientSecret else{
            return
        }
        
        let paymentIntentParams = STPPaymentIntentParams(clientSecret: clientSecret)
        paymentIntentParams.paymentMethodParams = paymentMethodParams
        
        paymentGatewayController.submitPayment(intent: paymentIntentParams){
            status, intent, error in
            
            switch status{
                case .failed:
                    message = "Failed"
                    isSuccess = false
                case .canceled:
                    message = "Cancalled"
                    isSuccess = false
                case .succeeded:
                    message = "Your payment has been successfully completed"
                    isSuccess = true
                    addToRecord()
                    sendEmailReceipt()
                    cart.cartList.removeAll()
            }
        }
        
    }
    
    private func addToRecord(){
        let db = Firestore.firestore()
        // add buy record, purchased = true
        for cartItem in cart.cartList{
            db.collection("artsRecord").document().setData(["accountId":profile.userId,
                            "accountName":profile.accountName,
                            "artName":cartItem.heading,
                            "price":cartItem.price,
                            "purchased":true])
            // add sold record, purchased = false
            db.collection("artsRecord").document().setData(["accountId":cartItem.artistID,
                            "accountName":cartItem.artist,
                            "artName":cartItem.heading,
                            "price":cartItem.price,
                            "purchased":false])
            artToSold(art: cartItem)
        }
    }
    
    private func artToSold(art:MarketCard){
        let db = Firestore.firestore()
        let docRef = db.collection("arts").document(art.id)
        docRef.setData(["artist":art.artist,
                        "description":art.description,
                        "image":art.image,
                        "name":art.heading,
                        "price":art.price,
                        "galleryShow":art.galleryShow,
                        "marketPlaceRevealed":false,
                        "artistID":art.artistID])
    }
    
    private func sendEmailReceipt(){
        var art_price = ""
                for i in cart.cartList{
                    art_price = i.heading+" "+i.price+"\n"
                }
        var subtotal_double = 0.0
                for i in cart.cartList{
                    subtotal_double += Double(i.price) ?? 0.0
                }
        let subtotal_string = subtotal_double.description
        let emailBody = "Thank you for your patronage!\nHere is your receipt:\n"+art_price+"Subtotal: "+subtotal_string+"\nDigital Grafitti"
        let smtp = SMTP(hostname: "smtp.gmail.com", email: "dg.digitalgrafitti@gmail.com", password: "nriqgggfcwsmacib")
        let buyer = Mail.User(name: profile.userId, email: profile.accountName)
        let group = Mail.User(name: "Digital Grafitti", email: "dg.digitalgrafitti@gmail.com")
        let mail = Mail(from: group, to: [buyer], subject: "Today's Purchase from Digital Grafitti", text: emailBody)
        smtp.send(mail){ (error) in
            if let error = error {
                print("email error")
                print(error)
            }
        }
        
    }
    
    var body: some View {
        VStack{
            List{
                ForEach(cart.cartList, id: \.heading){ item in
                    HStack{
                        Text(item.heading)
                        Spacer()
                        Text("$"+item.price)
                    }
                }

            HStack{
                Spacer()
                Text(String(format: "$%.2f", cart.cartTotal))
                Spacer()
            }
            Section{
                // Stripe CC TextField Here
                STPPaymentCardTextField.Representable.init(paymentMethodParams: $paymentMethodParams)
            } header:{
                Text("Payment Information")
            }
            HStack{
                Spacer()
                Button("Pay"){
                    pay()
                }.buttonStyle(.plain)
                Spacer()
            }
            Text(message)
                .font(.headline)
            }
            NavigationLink(isActive: $isSuccess, destination: {
                            Confirmation()
                        }, label: {
                            EmptyView()
                        })
            .navigationTitle("Checkout")
        }
    }
}

