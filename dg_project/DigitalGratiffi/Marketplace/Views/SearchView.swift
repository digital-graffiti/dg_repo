//
//  SearchView.swift
//  DigitalGratiffi
//
//  Created by Harvey Ho on 2021-11-07.
//

import SwiftUI

private var optionLabels = [
    "By Name of Art",
    "By Artist",
    "Less than Price",
    "Greater than Price",
    "Select an option"
]

private var searchQueryValue = ""
private var searchQueryFor = "name"

struct SearchBar: View {
    @State var searchText: String
    @State private var isEditing = false
    @State var userId: String
    @State var isAnon: Bool
    
        var body: some View {
            HStack {
     
                TextField("Search ...", text: $searchText)
                    .padding(7)
                    .padding(.horizontal, 25)
                    .background(Color(.systemGray6))
                    .cornerRadius(8)
                    .padding(.horizontal, 10)
                    .onTapGesture {
                        self.isEditing = true
                        searchQueryValue = searchText
                    }
     
                if isEditing {
                    Button(action: {
                        self.isEditing = false
                        print("SearchView: "+searchQueryFor+" "+searchText)
                    }) {
                        Text("Done")
                    }
                    .padding(.trailing, 10)
                    .transition(.move(edge: .trailing))
                    .animation(.default)
                } else{
                    NavigationLink(destination: MarketplaceView(qf: searchQueryFor, qv: searchText,uid: userId, a:isAnon)){
                        Label("", systemImage: "magnifyingglass")
                    }
                    .padding(.trailing, 10)
                    .transition(.move(edge: .trailing))
                    .animation(.default)
                    .onTapGesture {
                        searchText = ""
                        searchQueryFor = ""
                        searchQueryValue = ""
                    }
                }
            }
        }
 }


struct SearchView: View {
    @State var userId: String
    @State var isAnon: Bool
    @State var sqf: String = ""

    var body: some View {
        SearchBar(searchText: "",userId: userId,isAnon: isAnon)
        Picker(selection: $sqf, label: Text("Search Options")){
            Text(optionLabels[4]).tag("default")
            Text(optionLabels[0]).tag("name")
            Text(optionLabels[1]).tag("artist")
            Text(optionLabels[2]).tag("<=")
            Text(optionLabels[3]).tag(">=")
        }.onChange(of: sqf){newValue in
            searchQueryFor = newValue
            print(searchQueryFor)
        }
    }
}

