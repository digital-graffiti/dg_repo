//
//  SearchBar.swift
//  DigitalGratiffi
//
//  Created by Harvey Ho on 2021-12-07.
//

import Foundation

struct SearchBar: View {
    @State var searchText: String
    @State private var isEditing = false
    @State var userId: String
    @State var isAnon: Bool

        var body: some View {
            HStack {
     
                TextField("Search ...", text: $searchText)
                    .padding(7)
                    .padding(.horizontal, 25)
                    .background(Color(.systemGray6))
                    .cornerRadius(8)
                    .padding(.horizontal, 10)
                    .onTapGesture {
                        self.isEditing = true
                        searchQueryValue = searchText
                    }
     
                if isEditing {
                    Button(action: {
                        self.isEditing = false
                        print("SearchView: "+searchQueryFor+" "+searchText)
                    }) {
                        Text("Done")
                    }
                    .padding(.trailing, 10)
                    .transition(.move(edge: .trailing))
                    .animation(.default)
                } else{
                    NavigationLink(destination: MarketplaceView(qf: searchQueryFor, qv: searchText,uid: userId, a:isAnon)){
                        Label("", systemImage: "magnifyingglass")
                    }
                    .padding(.trailing, 10)
                    .transition(.move(edge: .trailing))
                    .animation(.default)
                    .onTapGesture {
                        searchText = ""
                        searchQueryFor = ""
                        searchQueryValue = ""
                    }
                }
            }
        }
 }
