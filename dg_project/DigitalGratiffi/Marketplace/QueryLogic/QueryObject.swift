//
//  QueryObject.swift
//  DigitalGratiffi
//
//  Created by Harvey Ho on 2021-12-07.
//

import Foundation
import Firebase
import FirebaseStorage

class QueryObject: ObservableObject{
    @Published var artsCards : [MarketCard] = []
    @Published var records: [Record] = []
    func getAllArtsForQuery(patronQueryFor: String, patronQueryValue: String, submittedUserId: String, anon: Bool){
        let db = Firestore.firestore()
        switch anon{
        case true:
            
            switch patronQueryFor{
            case "marketplaceRevealed":
                db.collection("arts").whereField("marketPlaceRevealed", isEqualTo: true)
                    .getDocuments(){(querySnapshot, err) in
                   if err == nil{ //check for errors
                       //no errors
                       if let snapshot = querySnapshot{
                           
                           DispatchQueue.main.async {
                               //get all the documents and create artsCards
                               self.artsCards = snapshot.documents.map { doc in
                                   return MarketCard(id: doc.documentID,
                                                     image: doc["image"] as? String ?? "",
                                                     heading: doc["name"] as? String ?? "",
                                                     artist: doc["artist"] as? String ?? "",
                                                     artistID: doc["artistID"] as? String ?? "",
                                                     price: doc["price"] as? String ?? "0.00",
                                                     description: doc["description"] as? String ?? "",
                                   galleryShow: doc["galleryShow"] as? Bool ?? false,
                                   marketplaceRevealed: doc["marketPlaceRevealed"] as? Bool ?? false)
                               }
                           }
                       }
                   } else {
                       print("Error")
                   }
               }
            case "name":
                db.collection("arts").whereField("marketPlaceRevealed", isEqualTo: true).whereField("name", isEqualTo: patronQueryValue)
                    .getDocuments(){(querySnapshot, err) in
                   if err == nil{ //check for errors
                       //no errors
                       if let snapshot = querySnapshot{
                           
                           DispatchQueue.main.async {
                               //get all the documents and create artsCards
                               self.artsCards = snapshot.documents.map { doc in
                                   return MarketCard(id: doc.documentID,
                                                     image: doc["image"] as? String ?? "",
                                                     heading: doc["name"] as? String ?? "",
                                                     artist: doc["artist"] as? String ?? "",
                                                     artistID: doc["artistID"] as? String ?? "",
                                                     price: doc["price"] as? String ?? "0.00",
                                                     description: doc["description"] as? String ?? "",
                                   galleryShow: doc["galleryShow"] as? Bool ?? false,
                                   marketplaceRevealed: doc["marketPlaceRevealed"] as? Bool ?? false)
                               }
                           }
                       }
                   } else {
                       print("Error")
                   }
               }
            case "artist":
                db.collection("arts").whereField("marketPlaceRevealed", isEqualTo: true).whereField("artist", isEqualTo: patronQueryValue)
                    .getDocuments(){(querySnapshot, err) in
                   if err == nil{ //check for errors
                       //no errors
                       if let snapshot = querySnapshot{
                           
                           DispatchQueue.main.async {
                               //get all the documents and create artsCards
                               self.artsCards = snapshot.documents.map { doc in
                                   return MarketCard(id: doc.documentID,
                                                     image: doc["image"] as? String ?? "",
                                                     heading: doc["name"] as? String ?? "",
                                                     artist: doc["artist"] as? String ?? "",
                                                     artistID: doc["artistID"] as? String ?? "",
                                                     price: doc["price"] as? String ?? "0.00",
                                                     description: doc["description"] as? String ?? "",
                                   galleryShow: doc["galleryShow"] as? Bool ?? false,
                                   marketplaceRevealed: doc["marketPlaceRevealed"] as? Bool ?? false)
                               }
                           }
                       }
                   } else {
                       print("Error")
                   }
               }
            case "<=":
                db.collection("arts").whereField("marketPlaceRevealed", isEqualTo: true)
                    .getDocuments(){(querySnapshot, err) in
                   if err == nil{ //check for errors
                       //no errors
                       if let snapshot = querySnapshot{
                           
                           DispatchQueue.main.async {
                               for doc in snapshot.documents{
                                   let docPrice = doc["price"] as? String ?? "0.00"
                                   let patronPrice:Float = Float(patronQueryValue) ?? 0.00
                                   if Float(docPrice) ?? 0.00 <= patronPrice{
                                       self.artsCards.append(
                                        MarketCard(id: doc.documentID,
                                                          image: doc["image"] as? String ?? "",
                                                          heading: doc["name"] as? String ?? "",
                                                          artist: doc["artist"] as? String ?? "",
                                                          artistID: doc["artistID"] as? String ?? "",
                                                          price: doc["price"] as? String ?? "0.00",
                                                          description: doc["description"] as? String ?? "",
                                                   galleryShow: doc["galleryShow"] as? Bool ?? false,
                                                   marketplaceRevealed: doc["marketPlaceRevealed"] as? Bool ?? false)
                                       )
                                   }
                               }
                           }
                       }
                   } else {
                       print("Error")
                   }
               }
            case ">=":
                db.collection("arts").whereField("marketPlaceRevealed", isEqualTo: true)
                    .getDocuments(){(querySnapshot, err) in
                   if err == nil{ //check for errors
                       //no errors
                       if let snapshot = querySnapshot{
                           
                           DispatchQueue.main.async {
                               //get all the documents and create artsCards
                               for doc in snapshot.documents{
                                   let docPrice = doc["price"] as? String ?? "0.00"
                                   let patronPrice:Float = Float(patronQueryValue) ?? 0.00
                                   if Float(docPrice) ?? 0.00 >= patronPrice{
                                       self.artsCards.append(
                                        MarketCard(id: doc.documentID,
                                                          image: doc["image"] as? String ?? "",
                                                          heading: doc["name"] as? String ?? "",
                                                          artist: doc["artist"] as? String ?? "",
                                                        artistID: doc["artistID"] as? String ?? "",
                                                          price: doc["price"] as? String ?? "0.00",
                                                          description: doc["description"] as? String ?? "",
                                                   galleryShow: doc["galleryShow"] as? Bool ?? false,
                                                   marketplaceRevealed: doc["marketPlaceRevealed"] as? Bool ?? false)
                                       )
                                   }
                               }
                           }
                       }
                   } else {
                       print("Error")
                   }
               }
            default:
                db.collection("arts").getDocuments(){(querySnapshot, err) in
                   if err == nil{ //check for errors
                       //no errors
                       if let snapshot = querySnapshot{
                           
                           DispatchQueue.main.async {
                               //get all the documents and create artsCards
                               self.artsCards = snapshot.documents.map { doc in
                                   return MarketCard(id: doc.documentID,
                                                     image: doc["image"] as? String ?? "",
                                                     heading: doc["name"] as? String ?? "",
                                                     artist: doc["artist"] as? String ?? "",
                                                     artistID: doc["artistID"] as? String ?? "",
                                                     price: doc["price"] as? String ?? "0.00",
                                                     description: doc["description"] as? String ?? "",
                                   galleryShow: doc["galleryShow"] as? Bool ?? false,
                                   marketplaceRevealed: doc["marketPlaceRevealed"] as? Bool ?? false)
                               }
                           }
                       }
                   } else {
                       print("Error")
                   }
               }
            }
            
        case false:
            
            switch patronQueryFor{
            case "name":
                db.collection("arts").whereField("artistID", isEqualTo: submittedUserId).whereField("name", isEqualTo: patronQueryValue)
                    .getDocuments(){(querySnapshot, err) in
                   if err == nil{ //check for errors
                       //no errors
                       if let snapshot = querySnapshot{
                           
                           DispatchQueue.main.async {
                               //get all the documents and create artsCards
                               self.artsCards = snapshot.documents.map { doc in
                                   return MarketCard(id: doc.documentID,
                                                     image: doc["image"] as? String ?? "",
                                                     heading: doc["name"] as? String ?? "",
                                                     artist: doc["artist"] as? String ?? "",
                                                     artistID: doc["artistID"] as? String ?? "",
                                                     price: doc["price"] as? String ?? "0.00",
                                                     description: doc["description"] as? String ?? "",
                                   galleryShow: doc["galleryShow"] as? Bool ?? false,
                                   marketplaceRevealed: doc["marketPlaceRevealed"] as? Bool ?? false)
                               }
                           }
                       }
                   } else {
                       print("Error")
                   }
               }
            case "artist":
                db.collection("arts").whereField("artistID", isEqualTo: submittedUserId).whereField("artist", isEqualTo: patronQueryValue)
                    .getDocuments(){(querySnapshot, err) in
                   if err == nil{ //check for errors
                       //no errors
                       if let snapshot = querySnapshot{
                           
                           DispatchQueue.main.async {
                               //get all the documents and create artsCards
                               self.artsCards = snapshot.documents.map { doc in
                                   return MarketCard(id: doc.documentID,
                                                     image: doc["image"] as? String ?? "",
                                                     heading: doc["name"] as? String ?? "",
                                                     artist: doc["artist"] as? String ?? "",
                                                     artistID: doc["artistID"] as? String ?? "",
                                                     price: doc["price"] as? String ?? "0.00",
                                                     description: doc["description"] as? String ?? "",
                                   galleryShow: doc["galleryShow"] as? Bool ?? false,
                                   marketplaceRevealed: doc["marketPlaceRevealed"] as? Bool ?? false)
                               }
                           }
                       }
                   } else {
                       print("Error")
                   }
               }
            case "<=":
                db.collection("arts").whereField("artistID", isEqualTo: submittedUserId)
                    .getDocuments(){(querySnapshot, err) in
                   if err == nil{ //check for errors
                       //no errors
                       if let snapshot = querySnapshot{
                           
                           DispatchQueue.main.async {
                               for doc in snapshot.documents{
                                   let docPrice = doc["price"] as? String ?? "0.00"
                                   let patronPrice:Float = Float(patronQueryValue) ?? 0.00
                                   if Float(docPrice) ?? 0.00 <= patronPrice{
                                       self.artsCards.append(
                                        MarketCard(id: doc.documentID,
                                                          image: doc["image"] as? String ?? "",
                                                          heading: doc["name"] as? String ?? "",
                                                          artist: doc["artist"] as? String ?? "",
                                                        artistID: doc["artistID"] as? String ?? "",
                                                          price: doc["price"] as? String ?? "0.00",
                                                          description: doc["description"] as? String ?? "",
                                                   galleryShow: doc["galleryShow"] as? Bool ?? false,
                                                   marketplaceRevealed: doc["marketPlaceRevealed"] as? Bool ?? false)
                                       )
                                   }
                               }
                           }
                       }
                   } else {
                       print("Error")
                   }
               }
            case ">=":
                db.collection("arts").whereField("artistID", isEqualTo: submittedUserId)
                    .getDocuments(){(querySnapshot, err) in
                   if err == nil{ //check for errors
                       //no errors
                       if let snapshot = querySnapshot{
                           
                           DispatchQueue.main.async {
                               //get all the documents and create artsCards
                               for doc in snapshot.documents{
                                   let docPrice = doc["price"] as? String ?? "0.00"
                                   let patronPrice:Float = Float(patronQueryValue) ?? 0.00
                                   if Float(docPrice) ?? 0.00 >= patronPrice{
                                       self.artsCards.append(
                                        MarketCard(id: doc.documentID,
                                                          image: doc["image"] as? String ?? "",
                                                          heading: doc["name"] as? String ?? "",
                                                          artist: doc["artist"] as? String ?? "",
                                                        artistID: doc["artistID"] as? String ?? "",
                                                          price: doc["price"] as? String ?? "0.00",
                                                          description: doc["description"] as? String ?? "",
                                                   galleryShow: doc["galleryShow"] as? Bool ?? false,
                                                   marketplaceRevealed: doc["marketPlaceRevealed"] as? Bool ?? false)
                                       )
                                   }
                               }
                           }
                       }
                   } else {
                       print("Error")
                   }
               }
            default:
                db.collection("arts").whereField("artistID", isEqualTo: submittedUserId).getDocuments(){(querySnapshot, err) in
                   if err == nil{ //check for errors
                       //no errors
                       if let snapshot = querySnapshot{
                           
                           DispatchQueue.main.async {
                               //get all the documents and create artsCards
                               self.artsCards = snapshot.documents.map { doc in
                                   return MarketCard(id: doc.documentID,
                                                     image: doc["image"] as? String ?? "",
                                                     heading: doc["name"] as? String ?? "",
                                                     artist: doc["artist"] as? String ?? "",
                                                     artistID: doc["artistID"] as? String ?? "",
                                                     price: doc["price"] as? String ?? "0.00",
                                                     description: doc["description"] as? String ?? "",
                                   galleryShow: doc["galleryShow"] as? Bool ?? false,
                                   marketplaceRevealed: doc["marketPlaceRevealed"] as? Bool ?? false)
                               }
                           }
                       }
                   } else {
                       print("Error")
                   }
               }
            } //end switch patronQueryFor
        }// end switch anon
   }//end func getAllArtsForQuery
    func getAllRecords(userId: String){
        let db = Firestore.firestore()
        let docRef = db.collection("artsRecord")
        docRef.whereField("accountId", isEqualTo: userId).getDocuments() {
            (querySnapshot, err) in
            //no errors
            if let snapshot = querySnapshot{
                
                DispatchQueue.main.async {
                    //get all the documents and create artsCards
                    self.records = snapshot.documents.map { doc in
                        return Record(id: doc.documentID,
                                      accountId: doc["accountId"] as? String ?? "",
                                      accountName: doc["accountName"] as? String ?? "",
                                      artName : doc["artName"] as? String ?? "",
                                      price : doc["price"] as? String ?? "",
                                      purchased : doc["purchased"] as? Bool ?? false)
                    }
                }
            }else {
                print("Error")
            }
        }
    } // end getAllRecords
} //end queryObejct
