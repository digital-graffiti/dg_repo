//
//  CheckoutIntentResponse.swift
//  DigitalGratiffi
//
//  Created by Harvey Ho on 2022-10-13.
//

import Foundation

struct CheckoutIntentResponse: Decodable {
    let clientSecret: String
}
