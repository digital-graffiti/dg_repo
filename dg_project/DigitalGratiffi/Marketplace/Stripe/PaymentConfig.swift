//
//  PaymentConfig.swift
//  DigitalGratiffi
//
//  Created by Harvey Ho on 2022-10-13.
//

import Foundation

class PaymentConfig {
    
    var paymentIntentClientSecret: String?
    static var shared: PaymentConfig = PaymentConfig()
    
    private init() { }
}
