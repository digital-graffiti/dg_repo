//
//  Cart.swift
//  DigitalGratiffi
//
//  Created by Harvey Ho on 2021-12-08.
//

import Foundation

class Cart: ObservableObject{
    @Published var cartList: [MarketCard] = []
    
    var cartTotal: Float{
        var total: Float = 0.0
        for cartItem in cartList{
            total += Float(cartItem.price) ?? 0.0
        }
        return total
    }
}
