//
//  Favorite.swift
//  DigitalGratiffi
//
//  Created by Harvey Ho on 2022-11-18.
//

import Foundation

class Favorite: ObservableObject{
    @Published var favoriteList: [MarketCard] = []
}
