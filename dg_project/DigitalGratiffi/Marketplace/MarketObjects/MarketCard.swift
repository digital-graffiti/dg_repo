//
//  MarketCard.swift
//  DigitalGratiffi
//
//  Created by Harvey Ho on 2021-12-07.
//

import Foundation

struct MarketCard: Identifiable, Hashable, Codable{
    var id : String
    var image: String
    var heading: String
    var artist: String
    var artistID: String
    var price: String
    var description: String
    var galleryShow: Bool
    var marketplaceRevealed: Bool
}
