//
//  Store.swift
//  DigitalGratiffi
//
//  Created by Harvey Ho on 2022-10-10.
//

import Foundation
import StoreKit

typealias Transaction = StoreKit.Transaction

public enum StoreError: Error {
    case failedVerification
}

class Store: ObservableObject{
    
    @Published private(set) var arts: [Product]
    @Published private(set) var purchasedArts: [Product] = []
    @Published private var queryObject = QueryObject()
    
    var updateListenerTask: Task<Void, Error>? = nil
    
    init(){
        // add icon assignment here later
        
        arts = []
        
        updateListenerTask = listenForTransactions()
        
        Task {
            //During store initialization, request products from the App Store.
            await requestProducts()

            //Deliver products that the customer purchases.
            await updateCustomerProductStatus()
        }
    }
    
    deinit {
        updateListenerTask?.cancel()
    }
    
    func listenForTransactions() -> Task<Void, Error> {
        return Task.detached {
            //Iterate through any transactions that don't come from a direct call to `purchase()`.
            for await result in Transaction.updates {
                do {
                    let transaction = try self.checkVerified(result)

                    //Deliver products to the user.
                    await self.updateCustomerProductStatus()

                    //Always finish a transaction.
                    await transaction.finish()
                } catch {
                    //StoreKit has a transaction that fails verification. Don't deliver content to the user.
                    print("Transaction failed verification")
                }
            }
        }
    }
    
    
    @MainActor
    func requestProducts() async{
        let artsFromFirestore = queryObject.artsCards
        var newArts : [Product] = []
        
        for newArt in artsFromFirestore{
            var tempArt : Product
            
            
        }
    }
    
    @MainActor
    func updateCustomerProductStatus() async{
        var purchasedArts : [Product] = []
        
        for await result in Transaction.currentEntitlements{
            do{
                let transaction = try checkVerified(result)
                if let art = arts.first(where: {$0.id == transaction.productID}){
                    purchasedArts.append(art)
                }
            }catch{
                print()
            }
        }
        self.purchasedArts = purchasedArts
    }
    
    func checkVerified<T>(_ result: VerificationResult<T>) throws -> T {
        //Check whether the JWS passes StoreKit verification.
        switch result {
        case .unverified:
            //StoreKit parses the JWS, but it fails verification.
            throw StoreError.failedVerification
        case .verified(let safe):
            //The result is verified. Return the unwrapped value.
            return safe
        }
    }
}

