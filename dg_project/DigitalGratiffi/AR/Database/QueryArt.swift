//
//  QueryArt.swift
//  DigitalGratiffi
//
//  Created by Scott McGhie on 2022-11-24.
//

import Foundation
import Firebase
import FirebaseStorage
import ARKit

var currentCards: [MarketCard] = []
var galleryCards: [MarketCard] = []
var dlNodePosition: [NodeData] = []

class QueryArt: ObservableObject{
    @Published var artsCards1: [MarketCard] = []
    @Published var artsCards2: [MarketCard] = []
    let db = Firestore.firestore()
    
    func getArtGalleries () {
        db.collection("artGalleries").document("ag1")
            .getDocument(){ (querySnapshot, err) in
                   DispatchQueue.main.async {
                       let aData = querySnapshot?.data()
                       let mapUrl = aData!["mapUrl"] as? String ?? ""
                       let name = aData!["name"] as? [String] ?? []
                       let id = aData!["id"] as? [String] ?? []
                       let positionX = aData!["positionX"] as? [Float] ?? []
                       let positionY = aData!["positionY"] as? [Float] ?? []
                       let positionZ = aData!["positionZ"] as? [Float] ?? []
                       let nodeX = aData!["nodeX"] as? [Float] ?? []
                       let nodeY = aData!["nodeY"] as? [Float] ?? []
                       let nodeZ = aData!["nodeZ"] as? [Float] ?? []
                       let extentX = aData!["extentX"] as? [Float] ?? []
                       dlNodePosition.removeAll()
                       for i in 0...id.count-1{
                           dlNodePosition.append(NodeData(name: name[i], id: id[i], positionX: positionX[i], positionY: positionY[i], positionZ: positionZ[i], nodeX: nodeX[i], nodeY: nodeY[i], nodeZ: nodeZ[i], extentX: extentX[i]))
                       }
                }
            }
    }

    func getArtGallery(){
        db.collection("arts")
            .getDocuments(){ [self](querySnapshot, err) in
               if err == nil{
                   if let snapshot = querySnapshot{
                       DispatchQueue.main.async {
                           self.artsCards1 = snapshot.documents.map { doc in
                               return MarketCard(id: doc.documentID,
                                                 image: doc["image"] as! String,
                                                 heading: doc["name"] as! String,
                                                 artist: doc["artist"] as! String,
                                                 artistID: doc["artistID"] as! String,
                                                 price: doc["price"] as! String,
                                                 description: doc["description"] as! String,
                               galleryShow: doc["galleryShow"] as! Bool,
                               marketplaceRevealed: doc["marketPlaceRevealed"] as! Bool)
                           }
                           for i in self.artsCards1{
                               currentCards.append(i)
                           }
                    }
                   }
               } else {
                   print("Error")
               }
       }
    }
    
    func getArtGallery (nodeIds: [String]) {
        galleryCards.removeAll()
        for node in nodeIds{
                db.collection("arts").document(node)
            .getDocument(){ (querySnapshot, err) in
            DispatchQueue.main.async {
                let aData = querySnapshot?.data()
                let id = querySnapshot?.documentID
                let image = aData!["image"] as? String
                let heading = aData!["name"] as? String
                let artist = aData!["artist"] as? String
                let artistID = aData!["artistID"] as? String
                let price = aData!["price"] as? String
                let description = aData!["description"] as? String
                let galleryShow = aData!["galleryShow"] as? Bool
                let marketplaceRevealed = aData!["marketPlaceRevealed"] as? Bool
                galleryCards.append(MarketCard(id: id!, image: image!, heading: heading!, artist: artist!, artistID: artistID!, price: price!, description: description!, galleryShow: galleryShow!, marketplaceRevealed: marketplaceRevealed!))
            }
        }
    }
    }
}
