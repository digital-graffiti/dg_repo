//
//  ViewController.swift
//  ArtGallery
//
//  Created by Scott McGhie on 2021-11-19.
//

import UIKit
import ARKit
import SwiftUI
import SceneKit
import FirebaseFirestore
import FirebaseCore
import FirebaseStorage

class AGViewController: UIViewController, ARSCNViewDelegate, ObservableObject{
    
    @IBOutlet weak var toDisplay: UIButton!
    @IBOutlet weak var resetB: UIButton!
    @IBOutlet weak var sceneView: ARSCNView!
    @IBOutlet weak var imageV: UIImageView!
    @IBOutlet weak var aName: UILabel!
    @IBOutlet weak var aArt: UILabel!
    @IBOutlet weak var savePosition: UIButton!
    @IBOutlet weak var loadPosition: UIButton!
    @IBOutlet weak var prevB: UIButton!
    @IBOutlet weak var nextB: UIButton!
    @IBOutlet weak var marketDetail: UIButton!
    
    var statusComplete: Bool = false
    var nodePosition: [NodeData] = []
    var anchorArray: [ARPlaneAnchor] = []
    var activeNode: SCNNode?
    var hitNodeZ: CGFloat!
    var hitNodeCoord: SCNVector3!
    var hitNode: SCNNode!
    var hitNode1: SCNNode!
    let configuration = ARWorldTrackingConfiguration()
    var node: SCNNode!
    var planeAnchor: ARPlaneAnchor!
    let omniNode = SCNNode()
    var image: UIImage?
    var aNode = SCNNode()
    var upload = Upload()
    var extentX: Float!
    var toDisplayCount = 0
    var currentA: Int = 0
    var nodeIds: [String] = []
    var artGalleries: [ArtGallery] = []
    var checkpoint: Bool = false
    var marketcard: MarketCard?
    var nodeCounter: Int = 0
    var currentNode: Int = 0
    @ObservedObject var queryArt = QueryArt()

    override func viewDidLoad() {
        super.viewDidLoad()
        queryArt.getArtGallery()
        self.configuration.planeDetection = .vertical
        self.sceneView.session.run(configuration)
        self.sceneView.delegate = self
        self.configuration.isLightEstimationEnabled = true
        for i in 0..<currentCards.count{
            if marketcard?.id == currentCards[i].id {
                currentA = i
            }
        }
        if anchorArray.isEmpty == true {
            imageV.image = UIImage(named: "xhair")
        }

        aName.text = currentCards[currentA].artist
        aArt.text = currentCards[currentA].heading
        retImage(node: currentA)
        currentNode = currentA
        
        let panRecognizer = UIPanGestureRecognizer(target: self, action: #selector(pan(panGesture:)))
        self.sceneView.addGestureRecognizer(panRecognizer)
        let pinchRecognizer = UIPinchGestureRecognizer(target: self, action: #selector(pinch))
        self.sceneView.addGestureRecognizer(pinchRecognizer)
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(tap))
        self.sceneView.addGestureRecognizer(tapRecognizer)
        
        let resetImg = UIImage(named: "reset")?.resized(to: CGSize(width:40, height:40))
        resetB.setImage(resetImg?.withRenderingMode(.alwaysTemplate), for: .normal)
        resetB.tintColor = .white
        
        let toDisplayImg = UIImage(named: "hang")?.resized(to: CGSize(width:40, height:40))
        toDisplay.setImage(toDisplayImg?.withRenderingMode(.alwaysTemplate), for: .normal)
        toDisplay.tintColor = .white
        
        let saveImg = UIImage(named: "save")?.resized(to: CGSize(width:35, height:35))
        savePosition.setImage(saveImg?.withRenderingMode(.alwaysTemplate), for: .normal)
        savePosition.tintColor = .white
        
        let dlImg = UIImage(named: "download")?.resized(to: CGSize(width:35, height:35))
        loadPosition.setImage(dlImg?.withRenderingMode(.alwaysTemplate), for: .normal)
        loadPosition.tintColor = .white
        
        let leftImg = UIImage(named: "left-chevron")?.resized(to: CGSize(width:40, height:40))
        prevB.setImage(leftImg?.withRenderingMode(.alwaysTemplate), for: .normal)
        prevB.tintColor = .white
        
        let rightImg = UIImage(named: "right-chevron")?.resized(to: CGSize(width:40, height:40))
        nextB.setImage(rightImg?.withRenderingMode(.alwaysTemplate), for: .normal)
        nextB.tintColor = .white
    }
    
    func retImage(node: Int){
        let url = URL(string: currentCards[node].image)!
        URLSession.shared.dataTask(with: url) { data, response, error in
                guard let data = data, error == nil else { return }
            DispatchQueue.main.async { [self] in
                    self.image = UIImage(data: data)
                    let imagePrev = self.image?.resized(to: CGSize(width:120, height:120))
                    marketDetail.setImage(imagePrev?.withRenderingMode(.alwaysOriginal), for: .normal)
                }
        }.resume()
    }
    
    @IBAction func prevB(_ sender: Any) {
        if currentA > 0{
            nextB.isEnabled = true
            currentA -= 1
        }else{
            prevB.isEnabled = false
        }
        updateMenu()
    }
    
    @IBAction func nextB(_ sender: Any) {
        if currentA < currentCards.count-1{
            prevB.isEnabled = true
            currentA += 1
            print(currentCards.count-1)
        }else{
            nextB.isEnabled = false
        }
        updateMenu()
    }
    
    func updateMenu (){
        retImage(node: currentA)
        aName.text = currentCards[currentA].artist
        aArt.text = currentCards[currentA].heading
    }

    @IBAction func resetB(_ sender: Any) {
        let config = ARWorldTrackingConfiguration()
        config.isLightEstimationEnabled = true
        anchorArray.removeAll()
        imageV.image = UIImage(named: "xhair")
        imageV.isHidden = false
        node?.enumerateChildNodes { (node, stop) in
            node.removeFromParentNode()
        }
        self.configuration.planeDetection = .vertical
        sceneView.session.run(config, options: [.removeExistingAnchors, .resetTracking])
        self.sceneView.session.run(configuration)
        planeAnchor = nil
        nodePosition.removeAll()
    }
    
    @IBAction func marketDetail(_ sender: Any) {
        let mc = currentCards[currentNode]
        @StateObject var cart = Cart()
        let hostingController = UIHostingController(rootView: MarketplaceDetailsView(isAnon: true, cart: cart, marketcard: mc))
        navigationController?.pushViewController(hostingController, animated: true)
    }
    
    @IBAction func savePosition(_ button: UIButton) {
        sceneView.session.getCurrentWorldMap { worldMap, error in
            guard let map = worldMap
                else { fatalError("Can't get current world map")}

            do {
                let data = try NSKeyedArchiver.archivedData(withRootObject: map, requiringSecureCoding: true)
                try data.write(to: self.mapSaveURL, options: [.atomic])
                let storage = Storage.storage()
                let storageRef = storage.reference()
                let mapRef = storageRef.child("world.map")
                let uploadTask = mapRef.putData(data, metadata: nil) { (metadata, error) in
                  guard let metadata = metadata else {
                    return
                  }
                  let size = metadata.size
                  mapRef.downloadURL { (url, error) in
                    guard let downloadURL = url else {
                      return
                    }
                  }
                }
            } catch {
                fatalError("Can't save map: \(error.localizedDescription)")
            }
        }
        do{
            let d = try PropertyListEncoder().encode(nodePosition)
            try d.write(to: self.nodeSaveURL, options: [.atomic])
            

        }catch{
            fatalError("Can't save map: \(error.localizedDescription)")
        }
        print("save button pressed")
        var artGallery = ArtGallery(mapUrl: "mapUrl", nodePosition: nodePosition)
        artGalleries.append(artGallery)
        upload.uploadGallery(aG: artGallery)
    }
    
    var mapDataFromFile: Data? {
        return try? Data(contentsOf: mapSaveURL)
    }

    var nodeDataFromFile: Data? {
        return try? Data(contentsOf: nodeSaveURL)
    }
    
    func downloadGallery(){
        print("load button pressed")
        /// - Tag: ReadWorldMap
        let worldMap: ARWorldMap = {
            guard let data = mapDataFromFile
                else { fatalError("Map data doesn't exist") }
            do {
                guard let worldMap = try NSKeyedUnarchiver.unarchivedObject(ofClass: ARWorldMap.self, from: data)
                    else { fatalError("No ARWorldMap in archive.") }
                return worldMap
            } catch {
                fatalError("Can't unarchive ARWorldMap from file data: \(error)")
            }
        }()
        nodePosition = dlNodePosition
        let configuration = self.configuration
        configuration.initialWorldMap = worldMap
        sceneView.session.run(configuration, options: [.resetTracking, .removeExistingAnchors])
        imageV.isHidden = true
        nodeIds.removeAll()
        for i in nodePosition {
            nodeIds.append(i.id)
        }
        nodeIds.reverse()
        queryArt.getArtGallery(nodeIds: nodeIds)
    }
    
    @IBAction func loadPosition (_ button: UIButton) {
        print("load button pressed")
        galleryCards.removeAll()
        //nodePosition.removeAll()
        //dlNodePosition.removeAll()
        let storage = Storage.storage()
        let storageRef = storage.reference()
        let mapRef = storageRef.child("world.map")
        nodeCounter = 0
        queryArt.getArtGalleries()
        let localURL = mapSaveURL

        let downloadTask = mapRef.write(toFile: localURL, completion:  { url, error in
          if let error = error {
            print("Map download error", error)
          } else {
              DispatchQueue.main.async {
                  self.downloadGallery()
              }
          }
        })
        downloadTask.observe(.progress) { snap in
          let perComplete = 100.0 * Double(snap.progress!.completedUnitCount)
            / Double(snap.progress!.totalUnitCount)
            
        print("% complete: ", perComplete)
            self.statusComplete = true
        }
        checkpoint = true
    }
    
    @IBAction func toDisplay(_ sender: Any) {
        if !anchorArray.isEmpty{
            node = activeNode
            let lastAnchor = anchorArray.last!
            aNode = augmentArt(planeAnchor: lastAnchor)

            if node.childNodes.count < 50 {
                node!.addChildNode(aNode)
                //node!.addChildNode(self.omniNode)
            }
            let name = aNode.name!
            let id = currentCards[currentA].id
            let positionX = node.worldPosition.x
            let positionY = node.worldPosition.y
            let positionZ = node.worldPosition.z
            let nodeW = node.scale.x
            let nodeH = node.scale.y
            let nodeZ = node.scale.z
            
            if toDisplayCount < 1{
                extentX = planeAnchor.planeExtent.width
            }
            toDisplayCount += 1

            let nodeP = NodeData(name: name, id: id, positionX: positionX, positionY: positionY, positionZ: positionZ, nodeX: nodeW, nodeY: nodeH, nodeZ: nodeZ, extentX: extentX)

            nodePosition.append(nodeP)
            imageV.isHidden = true
            print("display button pressed")
        }
    }
    
    func augmentArt(planeAnchor: ARPlaneAnchor) -> SCNNode {
        let cImage = image
        let imageHRatio = cImage!.size.height / cImage!.size.width
        var artNode = SCNNode()
        if nodePosition.isEmpty{
            artNode = SCNNode(geometry: SCNPlane(width: CGFloat(planeAnchor.planeExtent.width), height: (CGFloat(planeAnchor.planeExtent.width) * imageHRatio)))
        }else{
            artNode = SCNNode(geometry: SCNPlane(width: CGFloat(nodePosition[0].extentX), height: (CGFloat(nodePosition[0].extentX) * imageHRatio)))
        }
        artNode.name = currentCards[currentA].id
        artNode.geometry?.firstMaterial?.diffuse.contents = cImage
        artNode.geometry?.firstMaterial?.lightingModel = .lambert
        artNode.position = SCNVector3(planeAnchor.center.x, planeAnchor.center.y,planeAnchor.center.z)
        artNode.eulerAngles = SCNVector3(90.degToRad, -180.degToRad, 180.degToRad)
        nodeCounter += 1
        return artNode
    }
    
    func augmentArt1(planeAnchor: ARPlaneAnchor, nodes: NodeData ) -> SCNNode {
        let urlString = galleryCards[nodeCounter].image
        print("nodeCounter2", nodeCounter)
        let url = NSURL(string: urlString)! as URL
        if let imageData: NSData = NSData(contentsOf: url) {
            image = UIImage(data: imageData as Data)
        }
        let cImage = image
        let imageHRatio = cImage!.size.height / cImage!.size.width
        var artNode = SCNNode()
        artNode = SCNNode(geometry: SCNPlane(width: CGFloat(nodes.extentX), height: (CGFloat(nodes.extentX) * imageHRatio)))
        artNode.name = nodes.id
        artNode.geometry?.firstMaterial?.diffuse.contents = cImage
        artNode.geometry?.firstMaterial?.lightingModel = .lambert
        artNode.position = SCNVector3(planeAnchor.center.x, planeAnchor.center.y,planeAnchor.center.z)
        artNode.eulerAngles = SCNVector3(90.degToRad, -180.degToRad, 180.degToRad)
        nodeCounter += 1
        return artNode
    }

    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        if anchorArray.isEmpty {
            guard nodePosition.isEmpty
                else { return }
            planeAnchor = anchor as? ARPlaneAnchor
            anchorArray.append(planeAnchor)
            activeNode = node
            DispatchQueue.main.async{
                self.imageV.image = UIImage(named: "check")
            }
            checkpoint = false
        }else{
            guard checkpoint == true
                else { return }
            for nodes in nodePosition {
                planeAnchor = anchor as? ARPlaneAnchor
                activeNode = node
                aNode = augmentArt1(planeAnchor: planeAnchor!, nodes: nodes)
                aNode.position.x = nodes.positionX
                aNode.position.y = nodes.positionY
                aNode.position.z = nodes.positionZ
                aNode.scale.x = nodes.nodeX
                aNode.scale.y = nodes.nodeY
                aNode.scale.z = nodes.nodeZ
                node.addChildNode(aNode)
                //sceneView.scene.rootNode.addChildNode(self.omniNode)
                print("Vertical plane detected, ARPlaneAnchor created")
            }
        }
        checkpoint = false
    }
    
    lazy var mapSaveURL: URL = {
        do {
            return try FileManager.default
                .url(for: .documentDirectory,
                     in: .userDomainMask,
                     appropriateFor: nil,
                     create: true)
                .appendingPathComponent("world.map")
        } catch {
            fatalError("Can't get map file save URL: \(error.localizedDescription)")
        }
    }()
    
    lazy var nodeSaveURL: URL = {
        do {
            return try FileManager.default
                .url(for: .documentDirectory,
                     in: .userDomainMask,
                     appropriateFor: nil,
                     create: true)
                .appendingPathComponent("node.position")
        } catch {
            fatalError("Can't get node file save URL: \(error.localizedDescription)")
        }
    }()

    @objc func pan(panGesture: UIPanGestureRecognizer){
        let sceneView = panGesture.view as! ARSCNView
        let panLocation = panGesture.location(in: self.sceneView)
        if imageV.isHidden {
            switch panGesture.state{
                case .began:
                    let hitTest = sceneView.hitTest(panLocation)
                    if !hitTest.isEmpty{
                        let hitNodeResult = hitTest.first!
                        hitNode = hitNodeResult.node
                        hitNodeCoord = hitNodeResult.worldCoordinates
                        hitNodeZ = CGFloat(sceneView.projectPoint(hitNodeCoord).z)
                    }
                case .changed:
                    let newRealPosition = sceneView.unprojectPoint(SCNVector3(panLocation.x, panLocation.y, hitNodeZ!))
                    let panVector = SCNVector3(
                        newRealPosition.x - hitNodeCoord!.x,
                        newRealPosition.y - hitNodeCoord!.y,
                        newRealPosition.z - hitNodeCoord!.z)
                        hitNode.localTranslate(by:panVector)
                        self.hitNodeCoord = newRealPosition
                        let fuck = hitNode.name!
                        let index = nodePosition.firstIndex { $0.id == fuck }
                        if !(index == nil) {
                        nodePosition[index!].positionX = hitNode.position.x
                        nodePosition[index!].positionY = hitNode.position.y
                        nodePosition[index!].positionZ = hitNode.position.z
                        }
            default:
                break
            }
        }
    }
    
    @objc func tap(tapGesture: UITapGestureRecognizer){
        let sceneView = tapGesture.view as! ARSCNView
        let tapLocation = tapGesture.location(in: sceneView)
        let hitTest = sceneView.hitTest(tapLocation)
        guard let hitNodeResult = hitTest.first
            else{return}
        hitNode = hitNodeResult.node
        let fuck = hitNode.name!
        let index = currentCards.firstIndex { $0.id == fuck }
        retImage(node: index!)
        aName.text = currentCards[index!].artist
        aArt.text = currentCards[index!].heading
        currentNode = index!
    }
    
    @objc func pinch(pinchGesture: UIPinchGestureRecognizer){
        let sceneView = pinchGesture.view as! ARSCNView
        let pinchLocation = pinchGesture.location(in: sceneView)
        let hitTest = sceneView.hitTest(pinchLocation)
        if !hitTest.isEmpty {
            let hitNodeResult = hitTest.first!
            hitNode1 = hitNodeResult.node
            let pinchAction = SCNAction.scale(by: pinchGesture.scale, duration: 0)
            hitNode1.runAction(pinchAction)
            pinchGesture.scale = 1.0
            let fuck = hitNode1.name!
            let index = nodePosition.firstIndex { $0.id == fuck }
            if !(index == nil){
                nodePosition[index!].nodeX = hitNode1.scale.x
                nodePosition[index!].nodeY = hitNode1.scale.y
                nodePosition[index!].nodeZ = hitNode1.scale.z
            }
        }
    }
}

//extension AGViewController {
//    func renderer(_ renderer: SCNSceneRenderer,
//           updateAtTime time: TimeInterval) {
//        guard let lightEstimate = sceneView.session.currentFrame?.lightEstimate else { return }
//        self.omniNode.light = SCNLight()
//        self.omniNode.light?.type = .omni
//        self.omniNode.light?.intensity = lightEstimate.ambientIntensity * 0.80
//        self.omniNode.light?.temperature = lightEstimate.ambientColorTemperature
//    }
//}
