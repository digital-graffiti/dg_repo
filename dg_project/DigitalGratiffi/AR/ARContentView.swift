//
//  ContentView.swift
//  DigitalGratiffi
//
//  Created by Scott McGhie on 2021-11-19.
//


import SwiftUI
import RealityKit

struct ARContentView: View {
    
    var marketcard: MarketCard

    var body: some View {
        CustomController(marketcard: marketcard)
    }
}

struct CustomController: UIViewControllerRepresentable{
    @State var marketcard: MarketCard

    func makeUIViewController(context: UIViewControllerRepresentableContext<CustomController>) -> UIViewController {
        let storyboard = UIStoryboard(name: "ArtPreview", bundle: Bundle.main)
        let controller = storyboard.instantiateViewController(identifier: "ARViewController") as! ARViewController
        controller.marketcard = marketcard
        return controller
    }
    
    func updateUIViewController(_ uiViewController: UIViewController, context:
                                UIViewControllerRepresentableContext<CustomController>){
    }
}


