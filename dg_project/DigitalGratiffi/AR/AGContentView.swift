//
//  ContentView.swift
//  DigitalGratiffi
//
//  Created by Scott McGhie on 2021-11-19.
//


import SwiftUI
import RealityKit


struct AGContentView: View {

    var marketcard: MarketCard!

    var queryArt1 = QueryArt()

    var body: some View {
        CustomController1(marketcard: marketcard)
    }
}
struct CustomController1: UIViewControllerRepresentable {
    @State var marketcard: MarketCard
    var queryArt1 = QueryArt()

    func makeUIViewController(context: UIViewControllerRepresentableContext<CustomController1>) -> UIViewController {
        let storyboard = UIStoryboard(name: "ArtGallery", bundle: Bundle.main)
        queryArt1.getArtGallery()
        let controller = storyboard.instantiateViewController(identifier: "AGViewController") as! AGViewController
        controller.marketcard = marketcard
        return controller
    }
    
    func updateUIViewController(_ uiViewController: UIViewController, context:
                                UIViewControllerRepresentableContext<CustomController1>){
    }

}

