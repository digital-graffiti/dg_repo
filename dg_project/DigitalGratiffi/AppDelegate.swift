//
//  AppDelegate.swift
//  DigitalGratiffi
//
//  Created by Harvey Ho on 2021-10-28.
//

import UIKit
import SwiftUI
import Firebase
import Stripe

@main
class AppDelegate: UIResponder, UIApplicationDelegate{

    var window: UIWindow?
    @StateObject var profile = Profile()
    @StateObject var favorite = Favorite()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        FirebaseApp.configure()
        
        // Stripe publishable key
        StripeAPI.defaultPublishableKey = "pk_test_51LsAJvD83XAsTNGHwvIoWJlzqFvLWduV8M3wDPjcvGh9FNiN3ktqhn5QNYaFwMw55I8BkAzQT05TSlg6Ag9juT6P00wJ4eV6Dh"
        
        
        // Create the SwiftUI view that provides the window contents.
        let mainMenuView = MainView().environmentObject(profile).environmentObject(favorite)
        

        // Use a UIHostingController as window root view controller.
        let window = UIWindow(frame: UIScreen.main.bounds)
        window.rootViewController = UIHostingController(rootView: mainMenuView)
        self.window = window
        window.makeKeyAndVisible()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

}

