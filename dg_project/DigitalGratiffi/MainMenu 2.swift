//
//  MainMenu.swift
//  DigitalGratiffi
//
//  Created by Harvey Ho on 2021-10-28.
//

import SwiftUI

struct MainMenu: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct MainMenu_Previews: PreviewProvider {
    static var previews: some View {
        MainMenu()
    }
}
