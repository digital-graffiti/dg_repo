//
//  MainView.swift
//  DigitalGratiffi
//
//  Created by Harvey Ho on 2022-11-27.
//

import SwiftUI
import Firebase
import Stripe

struct MainView: View {
    @State var email = ""
    @State var password = ""
    @State var isAnon:Bool = false
    @State var userId = ""
    @EnvironmentObject var profile: Profile
    var isMarketplaceRevealed: String = "true"
    var queryFor:String = "marketPlaceRevealed"
    @StateObject private var viewModel = PortfolioHomePageViewModel()
    var body: some View {
        if(userId != ""){
//            NavigationView{
                TabView{
                    if(isAnon){
                        MarketplaceView(qf: queryFor, qv: isMarketplaceRevealed, uid: userId, a: isAnon).tabItem{
                            Label("Marketplace", systemImage:"bag")
                        }
                        AGContentView().tabItem{
                            Label("AR", systemImage:"eyeglasses")
                        }
                    } //end if isAnon
                    else{
                        MarketplaceView(qf: queryFor, qv: isMarketplaceRevealed, uid: userId, a: true).tabItem{
                            Label("Marketplace", systemImage:"bag")
                        }
                        MarketplaceView(qf: "artistID", qv: userId,uid: userId, a:isAnon).tabItem{
                            Label("Art Details", systemImage:"pencil")
                        }
                        AGContentView().tabItem{
                            Label("AR", systemImage:"eyeglasses")
                        }
                        PortfolioPage().tabItem{
                            Label("Portfolio", systemImage:"person")
                        }
                        RecordsView(uId: profile.userId).tabItem{
                            Label("Records", systemImage:"list.bullet")
                        }
                    } //end else
                }.accentColor(.white)
                    .onAppear{
                        let tabBarApperance = UITabBarAppearance()
                        tabBarApperance.configureWithOpaqueBackground()
                        UITabBar.appearance().scrollEdgeAppearance = tabBarApperance
                    } //end tabview
//            }
//            .navigationViewStyle(.stack)//end NavigationView
            
        } //end if userId != ""
        else{
            NavigationView{
                VStack{
                    TextField("Email",text:$email)
                        .textInputAutocapitalization(.never)
                        .padding()
                        .border(.gray)
                    SecureField("Password",text:$password)
                        .padding()
                        .border(.gray)
                    Button(action: {login()}){
                        Text("Sign In")
                    }
                    .buttonStyle(.bordered)
                    .padding(5)
                    Button(action:{loginAsPatron()}){
                        Text("Patron Sign In")
                    }
                    .buttonStyle(.bordered)
                    .padding(5)
                    NavigationLink(destination: CreateAccountView()){
                        Text("Create Account")
                    }
                    .buttonStyle(.bordered)
                } //end VStack
            } //end NavigationView
            .environmentObject(profile)
        } //end else
    }
    func login() {
        UserDefaults.standard.set(self.email, forKey: "email")
        Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
            if error != nil {
                print(error?.localizedDescription ?? "")
            } else {
                print("success")
                userId = result?.user.uid ?? ""
                UserDefaults.standard.set(self.userId, forKey: "userId")
                profile.userId = userId
                profile.accountName = Auth.auth().currentUser?.email ?? ""
                print(userId)
                print(isAnon)
            }
        }
        email = ""
        password = ""
    }
    
    func loginAsPatron(){
        Auth.auth().signInAnonymously { authResult, error in
            guard let user = authResult?.user else {
                print("fail to login as patron")
                return
            }
            isAnon = user.isAnonymous  //true
            userId = user.uid
        }
    }
}

struct MainView_Previews: PreviewProvider {
    static var previews: some View {
        MainView()
    }
}
