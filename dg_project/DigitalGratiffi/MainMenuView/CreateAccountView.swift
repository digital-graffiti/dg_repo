//
//  CreateAccountView.swift
//  DigitalGratiffi
//
//  Created by Harvey Ho on 2022-10-24.
//

import SwiftUI
import Firebase

struct CreateAccountView: View {
    @State var email = ""
    @State var password = ""
    var body: some View {
        TextField("Email",text:$email)
            .textInputAutocapitalization(.never)
        SecureField("Password",text:$password)
        Button(action: {createAccount()}){
            Text("Create Account")
        }
    }
    private func createAccount(){
        Auth.auth().createUser(withEmail: email, password: password){ (result, error) in
            if error != nil{
                print(error?.localizedDescription ?? "")
            }else {
                print("acount created")
            }
        }
        email = ""
        password = ""
    }
}

struct CreateAccountView_Previews: PreviewProvider {
    static var previews: some View {
        CreateAccountView()
    }
}
