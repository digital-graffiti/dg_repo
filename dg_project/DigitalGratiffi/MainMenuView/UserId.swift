//
//  UserId.swift
//  DigitalGratiffi
//
//  Created by Harvey Ho on 2022-10-24.
//

import Foundation

class UserId: ObservableObject{
    @Published var userId = ""
}
