//
//  Profile.swift
//  DigitalGratiffi
//
//  Created by Harvey Ho on 2022-10-24.
//

import Foundation
class Profile: ObservableObject{
    @Published var userId = ""
    @Published var accountName = ""
}
