//
//  MainMenuView.swift
//  DigitalGratiffi
//
//  Created by Harvey Ho on 2021-10-28.
//

import SwiftUI

struct MainMenuView: View {

    var isMarketplaceRevealed: String = "true"
    var queryFor:String = "marketplaceRevealed"
    var body: some View {

        NavigationView{
            VStack{
                NavigationLink(destination: MarketplaceView(qf: queryFor, qv: isMarketplaceRevealed)){
                    Text("Marketplace")
                }
                NavigationLink(destination: PortfolioPage()){
                    Text("PortfolioPage")
                }
                NavigationLink(destination: SelectArt()){
                    Text("AR")
                }
                NavigationLink(destination: SearchView()){
                    Text("Search")
                }
            }
        }
    }
}


struct MainMenuView_Previews: PreviewProvider {
    static var previews: some View {
        MainMenuView()
    }
}
