//
//  TabBar.swift
//  DigitalGratiffi
//
//  Created by Vladislav Mun on 2022-10-28.
//

import SwiftUI
struct TopBar : View{
    @Binding var selected: Int;
    var body : some View{
            VStack(spacing: 30){
                HStack(alignment: .center){
                    Button(action: {
                        
                    }){
                        Image(systemName: "chevron.left").font(.headline).foregroundColor(.white)
                    }
                    Text("Portfolio Page").font(.system(size: 20)).fontWeight(.semibold).foregroundColor(.white).frame(maxWidth: .infinity)
                    NavigationLink(destination: UploadImagePage(), label: {
                        Image(systemName: "plus").font(.headline).foregroundColor(.white)
                    })
                }
                VStack(alignment: .center){
                    Circle().foregroundColor(.red).frame(width: 70)
                    Text("Vladislav Mun").foregroundColor(.white).fontWeight(.semibold)
                }
                
                HStack(alignment: .center){
                    Button(action: {
                        self.selected = 0;
                    }) {
                        Text(Image(systemName: "grid")).foregroundColor(.white)
                        Text("Grid").fontWeight(.semibold).foregroundColor(.white)
                    }.frame(maxWidth: .infinity)
                    
                    
                    
                    Button(action: {
                        self.selected = 1;
                    }) {
                        Text(Image(systemName: "info.circle")).foregroundColor(.white)
                        Text("About Me").fontWeight(.semibold).foregroundColor(.white)
                    }.frame(maxWidth: .infinity)
                }
            }.padding()

    }
}
