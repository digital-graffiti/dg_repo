//
//  TextFieldWithTitle.swift
//  DigitalGratiffi
//
//  Created by Vladislav Mun on 2021-12-07.
//

import SwiftUI

struct TextFieldWithTitle: View {
    let text: Binding<String>;
    let width: CGFloat;
    let height: CGFloat;
    let title: String;
    var body: some View{
        VStack(){
            HStack{
                Text(title).font(.system(size: 17, design: .rounded)).fontWeight(.semibold).padding(.leading, 25)
                Spacer()
            }

            TextEditor(text: text).frame(width: width-40, height: height)
                .padding(3)
                .overlay(
                RoundedRectangle(cornerRadius: 15)
                    .stroke(Color.gray, lineWidth: 1)
            )
        }

    }
}


