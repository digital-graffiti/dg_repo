//
//  ModalFieldsInformation.swift
//  DigitalGratiffi
//
//  Created by Vladislav Mun on 2021-12-07.
//

import SwiftUI

struct ModalFieldsInformation: View {
    let fieldName: String
    let fieldValue: String
    var body: some View {
        HStack{
            Text(fieldName).font(.system(.title2, design: .rounded)).fontWeight(.medium)
            Spacer()
        }
        HStack{
            Text(fieldValue)
            Spacer()
        }.padding(.bottom, 10)
    }
}

