//
//  ButtonImageRow.swift
//  DigitalGratiffi
//
//  Created by Vladislav Mun on 2021-12-06.
//

import SwiftUI
struct SheetView: View {
    @StateObject private var viewModel = PortfolioHomePageViewModel()
    @Environment(\.presentationMode) var presentationMode
    let rows = Array(repeating: GridItem(.flexible(), spacing: 20), count: 1)
    let imgObject: ImageModel
    
    var body: some View {
        
        GeometryReader{geometry in
            let size = geometry.size
            VStack(alignment: .center, spacing: 15){
                // Custom Bar For Sheet
                HStack(alignment: .center){
                    Button(action: {
                        presentationMode.wrappedValue.dismiss()
                    }){
                        Image(systemName: "chevron.left").font(.headline).foregroundColor(.white)
                    }
                    Text(imgObject.artName).font(.system(size: 20)).fontWeight(.semibold).foregroundColor(.white).frame(maxWidth: .infinity)
                    Button(action: {}){
                        Image(systemName: "plus").font(.headline).foregroundColor(.black)
                    }
                }.padding(EdgeInsets.init(top: 10, leading: 5, bottom: 10, trailing: 5))
                
                // Art Image
                HStack(alignment: .center){
                    AsyncImage(url: URL(string: imgObject.urlToImage)) { image in
                        image.resizable().aspectRatio(contentMode: .fill).cornerRadius(15)
                    } placeholder: {
                        ProgressView()
                    }
                    .frame(width: size.width, height: size.height/2.2)
                    .cornerRadius(10)
                }

                
                // Art Information
                ModalFieldsInformation(fieldName: "Description:", fieldValue: imgObject.description)
                ModalFieldsInformation(fieldName: "Done By:", fieldValue: imgObject.artistName)
                
                ScrollView(.horizontal, showsIndicators: false) {
                    LazyHGrid(rows: rows,spacing: 10) {
                        ForEach(imgObject.tags, id: \.self){tag in
                            Text(tag)
                                .padding(8)
                                .foregroundColor(.black)
                                .background(.white)
                                .cornerRadius(15)
                                
                        }
                    }
                }
                .frame(width: size.width-40)

                Spacer()
            }
        }.padding(20).background(.black).ignoresSafeArea(.all)
    }
}

struct ButtonImageRow: View {
    @StateObject private var viewModel = PortfolioHomePageViewModel()
    
    let urlToImage: String
    let imageObj: ImageModel
    let imgHeight: CGFloat
    let imgWidth: CGFloat
    
    var body: some View {
        Button(
            action: {
                print(urlToImage)
                viewModel.showingSheet.toggle()
            },
            label: {
                AsyncImage(url: URL(string: self.urlToImage)) { image in
                    image.resizable().aspectRatio(contentMode: .fill).cornerRadius(15)
                } placeholder: {
                    ProgressView()
                }
                .frame(width: imgWidth, height: imgHeight)
                .cornerRadius(15)
            }
        )
        .sheet(isPresented: $viewModel.showingSheet) {
            SheetView(imgObject: imageObj)
        }
    }
}

