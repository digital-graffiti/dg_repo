//
//  SingleButtonRow.swift
//  DigitalGratiffi
//
//  Created by Vladislav Mun on 2021-12-06.
//

import SwiftUI

struct SingleButtonRow: View {
    let urlToImage: String
    var body: some View {
        Button(
            action: {},
            label: {
                AsyncImage(url: URL(string: self.urlToImage)) { image in
                    image.resizable()
                } placeholder: {
                    Color.red
                }
                .frame(width: 300, height: 200)
                .cornerRadius(15)
                .scaledToFit()
            }
        )
    }
}
