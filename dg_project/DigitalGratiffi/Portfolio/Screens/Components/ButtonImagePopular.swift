//
//  ButtonImagePopular.swift
//  DigitalGratiffi
//
//  Created by Vladislav Mun on 2021-12-06.
//

import SwiftUI

struct ButtonImagePopular: View {
    let urlToImage: String
    var body: some View {
        Button(
            action: {
                print(urlToImage)
            },
            label: {
                AsyncImage(url: URL(string: self.urlToImage)) { image in
                    image.resizable().aspectRatio(contentMode: .fill)
                } placeholder: {
                    Color.red
                }
                .frame(width: 150, height: 100)
                .cornerRadius(15)
                //.scaledToFit()
            }
        )
    }
}
