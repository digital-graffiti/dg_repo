//
//  TagsComponent.swift
//  DigitalGratiffi
//
//  Created by Vladislav Mun on 2022-11-28.
//

import SwiftUI

struct TagsComponent: View {
    let width: CGFloat
    let height: CGFloat
    let rows = Array(repeating: GridItem(.flexible(), spacing: 20), count: 1)
    @State private var showingAlertTagIsValid = false
    @State private var showingAlertImageExist = false
    @State private var tag: String = "";
    var vm: UploadImagePageViewModel
    
    var body: some View {
        VStack(){
            HStack{
                Text("Tags").font(.system(size: 17, design: .rounded)).fontWeight(.semibold).padding(.leading, 25)
                Spacer()
            }
            
            TextEditor(text: $tag).frame(width: width-40, height: 40)
                .padding(3)
                .overlay(
                    RoundedRectangle(cornerRadius: 15)
                        .stroke(Color.gray, lineWidth: 1)
                )
            
            HStack{
                // Add Tag
                Button(
                    action:{
                        if(tag==""){
                                showingAlertTagIsValid = true
                        }else{
                            vm.tags.append(tag);
                            vm.tags = vm.tags.removeDuplicates()
                            tag = ""
                        }
                    },
                    label:{
                        HStack{
                            Text("Add Tag")
                            Image(systemName: "plus.circle")
                        }
                        .frame(width: width/2-20 ,height:60)
                        .background(.black)
                        .foregroundColor(.white)
                        .font(.system(size: 17, weight: .bold, design: .default))
                        .overlay(
                            RoundedRectangle(cornerRadius: 15)
                                .stroke(Color.white, lineWidth: 1)
                        )
                        
                    }
                )
                .alert("Cannot Add Empty Tag", isPresented: $showingAlertTagIsValid) {
                    Button("OK", role: .cancel) { }
                }
                .padding(.bottom, 20)
                
                // Predict tags button
                Button(
                    action:{
                        vm.isLoading = true
                        if(vm.image == nil){
                            showingAlertImageExist = true
                        }else{
                            vm.predictTags()
                            vm.tags = vm.tags.removeDuplicates()
                        }
                        
                    },
                    label:{
                        HStack{
                            if(vm.isLoading){
                                ProgressView()
                            }else{
                                Text("Preidict")
                                Image(systemName: "wand.and.rays")
                            }
                        }
                        .frame(width: width/2-20 ,height:60)
                        .background(.black)
                        .foregroundColor(.white)
                        .font(.system(size: 17, weight: .bold, design: .default))
                        .overlay(
                            RoundedRectangle(cornerRadius: 15)
                                .stroke(Color.white, lineWidth: 1)
                        )
                    }
                )
                .alert("No Image To Predict On", isPresented: $showingAlertImageExist) {
                    Button("OK", role: .cancel) { }
                }
                .padding(.bottom, 20)
            }.padding(.top, 10)
            
            // This is how tags are being displayed on the upload screen
            ScrollView(.horizontal, showsIndicators: false) {
                LazyHGrid(rows: rows,spacing: 10) {
                    ForEach(vm.tags, id: \.self){tag in
                        Text(tag)
                            .padding(8)
                            .foregroundColor(.black)
                            .background(.white)
                            .cornerRadius(15)
                            .contextMenu(menuItems: {
                                Button(action: {
                                    withAnimation(.default){
                                        let indexToRemove = vm.tags.firstIndex(where: {$0 == tag}) ?? 0
                                        vm.tags.remove(at: indexToRemove)
                                    }}, label: {Label("Remove Tag", systemImage: "trash")})
                            })
                            
                    }
                }
            }
            .frame(width: width-40)
        }
    }
}
