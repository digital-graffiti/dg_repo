//
//  PortfolioHomePage.swift
//  DigitalGratiffi
//
//  Created by Vladislav Mun on 2021-12-03.
//

import SwiftUI

struct PortfolioHomePage: View{
    //ViewModel for the Portfolio Page
    @ObservedObject private var viewModel = PortfolioHomePageViewModel()
    
    //Create Rows and Columns for the Grid
    let rows = Array(repeating: GridItem(.flexible(), spacing: 20), count: 1)
    let columns = Array(repeating: GridItem(.flexible(), spacing: 5), count: 2)
    
    //States and Namesapces available might need to move them to the ViewModel
    @State var isImgPresented = false
    @State var selectedImg: ImageModel = ImageModel(id: "", artistId: "", artistName: "", artName: "", description: "", artPrice: "", urlToImage: "", position: 0, isPinned: false, tags: [],  isShownOnGallery: false, isShownOnMarketplace: false)
    @Namespace var animation
    
    
    var body: some View{
        GeometryReader{ geometry in
            let size = geometry.size
            
            ScrollView() {
                
                /// START of PINNED section
                if(viewModel.pinnedImages.isEmpty){

                }else{
                    Text("Pinned:")
                        .foregroundColor(.white)
                        .foregroundColor(.white)
                        .font(.headline)
                        .fontWeight(.semibold)
                        .frame(width: size.width-10, alignment: .leading)
                    
                    ScrollView(.horizontal) {
                        LazyHGrid(rows: rows,spacing: 10) {
                            ForEach(viewModel.pinnedImages){ img in
                                ButtonImageRow(urlToImage: img.urlToImage, imageObj: img, imgHeight: size.height/2, imgWidth: size.width-10)
                                //MOVING OBJECT
                                .onDrag ({
                                    viewModel.currentImage = img
                                    return NSItemProvider(contentsOf: URL(string: "\(img.id)")!)!
                                })
                                .onDrop(of: [.url], delegate: DropViewDelegate(image: img, vm: viewModel))
                                
                                //CONTEXT MENU WITH EDITING OPTIONS
                                .contextMenu(menuItems: {
                                    Button(action: {
                                        withAnimation(.default){
                                            viewModel.removeFromPinned(image: img)
                                        }}, label: {Label("Unpin Art", systemImage: "pin.slash")})
                                    Button(action: {
                                        withAnimation(.default){
                                            let indexToRemove = viewModel.images.firstIndex(where: {$0.id == img.id}) ?? 0
                                                viewModel.images.remove(at: indexToRemove)
                                        }}, label: {Label("Delete", systemImage: "trash")})
                                })
                            }
                        }
                    }.padding(2)
                }
                /// END of PINNED section
                
                /// START of the ART section
                Text("Arts:")
                    .foregroundColor(.white)
                    .foregroundColor(.white)
                    .font(.headline)
                    .fontWeight(.semibold)
                    .frame(width: size.width-10, alignment: .leading)
                
                LazyVGrid(columns: columns, spacing: 20) {
                    ForEach(viewModel.images){img in
                        ButtonImageRow(urlToImage: img.urlToImage, imageObj: img, imgHeight: size.width/2-20, imgWidth: size.width/2-20)
                        //MOVING OBJECT
                        .onDrag ({
                            viewModel.currentImage = img
                            return NSItemProvider(contentsOf: URL(string: "\(img.id)")!)!
                        })
                        .onDrop(of: [.url], delegate: DropViewDelegate(image: img, vm: viewModel))
                        
                        //CONTEXT MENU WITH EDITING OPTIONS
                        .contextMenu(menuItems: {
                            Button(action: {
                                withAnimation(.default){
                                    viewModel.addToPinned(image: img)
                                }
                            },label: {Label("Pin Art", systemImage: "pin")})
                            Button(action: {
                                withAnimation(.default){
                                    let indexToRemove = viewModel.images.firstIndex(where: {$0.id == img.id}) ?? 0
                                        viewModel.images.remove(at: indexToRemove)
                                }}, label: {Label("Delete", systemImage: "trash")})
                        })
                    }
                }
                /// END of the ART Section
            }
        }
        // Make an API Call to GET ART
        .onAppear{viewModel.getDataFromFirebase()}
        
    }
}

struct PortfolioHomePage_Previews: PreviewProvider {
    static var previews: some View {
        PortfolioHomePage()
    }
}
