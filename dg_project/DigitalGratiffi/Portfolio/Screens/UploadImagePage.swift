//
//  UploadImagePage.swift
//  DigitalGratiffi
//
//  Created by Vladislav Mun on 2021-12-03.
//

import SwiftUI
import Firebase
import FirebaseStorage
import Foundation


struct UploadImagePage: View{
    @StateObject private var viewModel = UploadImagePageViewModel()
    //@ObservedObject private var pageModel = PortfolioPageViewModel()
    @State private var showAlertIncorrectFields = false
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    var body: some View {
        GeometryReader{proxy in
            let size = proxy.size
            ScrollView{
                VStack(){
                    //Image and Image PlaceHolder
                    ZStack{
                        Spacer().fullScreenCover(isPresented: $viewModel.isImagePickerPresented , content: {
                            ImagePicker(image: $viewModel.image)
                        })
                        if let image = viewModel.image{
                            Image(uiImage: image)
                                .resizable()
                                .scaledToFill()
                                .frame(width: size.width-10, height: 250)
                                .cornerRadius(15)
                        }else{
                            ZStack{
                                RoundedRectangle(cornerRadius: 15)
                                    .stroke(Color.white, style: StrokeStyle(lineWidth: 2, dash: [5]))
                                    .frame(width: size.width-10, height: 250)
                                    
                                Image(systemName: "photo").foregroundColor(Color.white)
                            }
                        }
                    }
                    //HStack To Scan and Open Gallery
                    HStack{
                        // Scan Button
                        Button(
                            action:{
                                UIApplication.shared.windows.filter({$0.isKeyWindow}).first?.rootViewController?.present(viewModel.getDocumentCameraViewController(), animated: true, completion: nil)
                            },
                            label:{
                                HStack{
                                    Text("Scan")
                                    Image(systemName: "scanner")
                                }
                                .frame(width: size.width/2-20 ,height:60)
                                .background(.black)
                                .foregroundColor(.white)
                                .font(.system(size: 17, weight: .bold, design: .default))
                                .overlay(
                                    RoundedRectangle(cornerRadius: 15)
                                        .stroke(Color.white, lineWidth: 1)
                                )

                            }
                        ).padding(.bottom, 20)
                        
                        //Open Gallery Button
                        Button(
                            action:{
                                viewModel.isImagePickerPresented.toggle()
                            },
                            label:{
                                HStack{
                                    Text("Gallery")
                                    Image(systemName: "photo.stack")
                                }
                                .frame(width: size.width/2-20 ,height:60)
                                .background(.black)
                                .foregroundColor(.white)
                                .font(.system(size: 17, weight: .bold, design: .default))
                                .overlay(
                                    RoundedRectangle(cornerRadius: 15)
                                        .stroke(Color.white, lineWidth: 1)
                                )

                            }
                        ).padding(.bottom, 20)
                    }.padding(.top, 10)

                    Group{
                        TextFieldWithTitle(text: $viewModel.artName, width: size.width, height: 40, title:"(*) Name of the work")
                        TextFieldWithTitle(text: $viewModel.desc, width: size.width, height: 100, title:"(*) Description of the work")
                        TagsComponent(width: size.width, height: size.height, vm: viewModel)
                        TextFieldWithTitle(text: $viewModel.price, width: size.width, height: 40, title:"(*) Price")
                    }
  
                    Toggle(
                        isOn: $viewModel.isOnGallery,
                        label: {
                            Text("Show on Gallery").font(.system(size: 17, design: .rounded)).fontWeight(.medium)
                        }
                    ).padding(25)
                    Toggle(
                        isOn: $viewModel.isOnMarketplace,
                        label: {
                            Text("Show on Marketplace").font(.system(size: 17, design: .rounded)).fontWeight(.medium)
                        }
                    ).padding(25)
                                    
                    Spacer()
                    
                    Button(
                        action:{
                            if(viewModel.desc != "" || viewModel.image != nil || viewModel.artName != ""){
                                viewModel.uploadWork()
                                presentationMode.wrappedValue.dismiss()
                            }else{
                                showAlertIncorrectFields = true
                            }
                            
                        },
                        label:{
                            Text("Upload Image To Portfolio")
                                .frame(width: 300 ,height:60)
                                .background(.black)
                                .foregroundColor(.white)
                                .font(.system(size: 17, weight: .bold, design: .default))
                                .overlay(
                                    RoundedRectangle(cornerRadius: 15)
                                        .stroke(Color.white, lineWidth: 1)
                                )
                        }
                    )
                    .alert("Please Fill All Necessary Fields (*)", isPresented: $showAlertIncorrectFields) {
                        Button("OK", role: .cancel) { }
                    }
                    .padding(.bottom, 20)
                    
                }.navigationTitle("Upload New Work")
            }
        }
    }
}



