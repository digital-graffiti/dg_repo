//
//  DropViewDelegate.swift
//  portfolio_new
//
//  Created by Vladislav Mun on 2022-10-26.
//

import Foundation
import SwiftUI

struct DropViewDelegate : DropDelegate{
    var image: ImageModel
    var vm: PortfolioHomePageViewModel
    
    
    func performDrop(info: DropInfo) -> Bool {
        return true
    }
    
    func dropExited(info: DropInfo) {
        //call update the positions when drop is finished
        vm.updatePositions()
    }
    
    func dropEntered(info: DropInfo) {
        let fromIndex = vm.images.firstIndex{(image) -> Bool in
            return image.id == vm.currentImage?.id
        } ?? 0
        
        let toIndex =  vm.images.firstIndex{(image) -> Bool in
            return image.id == self.image.id
        } ?? 0
        
        if fromIndex != toIndex{
            withAnimation(.default){
                let fromImage = vm.images[fromIndex]
                vm.images[fromIndex] = vm.images[toIndex]
                vm.images[toIndex] = fromImage
            }

        }
    }
}
