//
//  PortfolioHomePageViewModel.swift
//  DigitalGratiffi
//
//  Created by Vladislav Mun on 2021-12-04.
//
//  This is the View Model of the Custom Grid
//  Main Functionalities are: retrieve data, update data and save changes made on the main grid
//  This might turn into the edit view instead of main view in the future

import Foundation
import Firebase
import FirebaseStorage
final class PortfolioHomePageViewModel: ObservableObject{
    @Published var images : [ImageModel] = [ImageModel]()
    @Published var layoutImages = [[ImageModel]]()
    @Published var showingSheet = false
    
    // New additions
    @Published var pinnedImages: [ImageModel] = []
    @Published var isImgPresented: Bool = false
    @Published var currentImage : ImageModel?
    // End of new additions revision them later
    private var db = Firestore.firestore()
    
    // Fetch data from the Firebase storage
    func getDataFromFirebase(){
        images = []
        let userID = UserDefaults.standard.string(forKey: "userId") ?? ""
        
        db
            .collection("arts")
            .whereField("artistID", isEqualTo: userID)
            .order(by: "position", descending: false)
            .addSnapshotListener{ [self](querySnapshot, err) in
            guard let documents = querySnapshot?.documents else{
                print("No Documents Found")
                return
            }
            
            self.images = documents.map{(snapshot) -> ImageModel in
                let data = snapshot.data()
                let id = snapshot.documentID
                let artistId = data["artistID"] as? String ?? ""
                let artistName = data["artist"] as? String ?? ""
                let artName = data["name"] as? String ?? ""
                let description = data["description"] as? String ?? ""
                let artPrice = data["price"] as? String ?? ""
                let urlToImage = data["image"] as? String ?? ""
                let isShownOnGallery = data["galleryShow"] as? Bool ?? false
                let isShownOnMarketPlace = data["marketPlaceRevealed"] as? Bool ?? false
                let position = data["position"] as? Int ?? 0
                let isPinned = data["isPinned"] as? Bool ?? false
                let tags = data["tags"] as? [String] ?? []
                
                return ImageModel(id: id,
                                  artistId: artistId,
                                  artistName: artistName,
                                  artName: artName,
                                  description: description,
                                  artPrice: artPrice,
                                  urlToImage: urlToImage,
                                  position: position,
                                  isPinned: isPinned,
                                  tags: tags,
                                  isShownOnGallery: isShownOnGallery,
                                  isShownOnMarketplace: isShownOnMarketPlace
                )
            }
                
                for i in self.images{
                    if i.isPinned{
                        let indexToRemove = self.images.firstIndex(where: {$0.id == i.id}) ?? 0
                        self.pinnedImages.append(i)
                        self.images.remove(at: indexToRemove)
                    }
                }
                
                self.pinnedImages = self.pinnedImages.removeDuplicates()
        }
    }
    
    // Function to Add Image to Pinned Stack
    func addToPinned(image: ImageModel){
        let indexToPin = self.images.firstIndex(where: {$0.id == image.id}) ?? 0
        var imageToPin = self.images[indexToPin]
        imageToPin.isPinned = true
        
        let artRef = db.collection("arts").document(image.id)
        artRef.updateData(["isPinned": imageToPin.isPinned]) { error in
            if let error = error {
                print("Error updating document: \(error)")
            } else {
                print("Document successfully updated!")
            }
        }
        
        self.images.remove(at: indexToPin)
        self.pinnedImages.insert(imageToPin, at: 0)
        updatePositions()
    }
    
    // Function to Remove Image From Pinned Stack
    func removeFromPinned(image: ImageModel){
        let indexToUnpin = self.pinnedImages.firstIndex(where: {$0.id == image.id}) ?? 0
        var imageToUnpin = self.pinnedImages[indexToUnpin]
        imageToUnpin.isPinned = false
        
        let artRef = db.collection("arts").document(image.id)
        artRef.updateData(["isPinned": imageToUnpin.isPinned]) { error in
            if let error = error {
                print("Error updating document: \(error)")
            } else {
                print("Document successfully updated!")
            }
        }
        
        self.pinnedImages.remove(at: indexToUnpin)
        self.images.append(imageToUnpin)
        updatePositions()
    }
    
    // Function To Update positions of the tiles
    func updatePositions(){
        let batch = db.batch()
        
        for img in images{
            let updatedIndex = self.images.firstIndex(where: {$0.id == img.id}) ?? 0
            let artRef = db.collection("arts").document(img.id)
            batch.updateData(["position": updatedIndex], forDocument: artRef)
        }
        
        batch.commit() { err in
            if let err = err {
                print("Error writing batch \(err)")
            } else {
                print("Batch write succeeded.")
            }
        }

    }
}


// Extension to remove duplicates from the arrays
extension Array where Element:Equatable {
    func removeDuplicates() -> [Element] {
        var result = [Element]()

        for value in self {
            if result.contains(value) == false {
                result.append(value)
            }
        }

        return result
    }
}
