//
//  UploadImagePageViewModel.swift
//  DigitalGratiffi
//
//  Created by Vladislav Mun on 2021-12-03.
//

import Foundation
import SwiftUI
import Firebase
import FirebaseStorage
import VisionKit
import UIKit



final class UploadImagePageViewModel: NSObject, ObservableObject{
    @Published var isImagePickerPresented = false
    @Published var artName = ""
    @Published var desc = ""
    @Published var image: UIImage?
    @Published var artistId = ""
    @Published var artistName = ""
    @Published var isOnGallery = false
    @Published var isOnMarketplace = false
    @Published var isLoading = false
    @Published var tags:[String] = []
    @Published var price = ""
    @Published var errorMessage: String?
    @Published var imageArray: [UIImage] = []
    
    // Function to retrieve scanner
    func getDocumentCameraViewController() -> VNDocumentCameraViewController {
        // Clear an image array before showing up scanner view
        self.imageArray = []
        let vc = VNDocumentCameraViewController()
        vc.delegate = self
        return vc
    }
    
    func removeImage(image: UIImage) {
        imageArray.removeAll{$0 == image}
    }
    
    // Function To Predict Tags
    func predictTags(){
        // Load Image and Convert to Base64
        let image = self.image// path to image to upload ex: image.jpg
        let imageData = image?.jpegData(compressionQuality: 0.3)
        let fileContent = imageData?.base64EncodedString()
        let postData = fileContent!.data(using: .utf8)

        // Initialize Inference Server Request with sb5cN4QwGnDTvbXgWFoy, Model, and Model Version
        var request = URLRequest(url: URL(string: "https://classify.roboflow.com/dg-2/1?api_key=sb5cN4QwGnDTvbXgWFoy&name=YOUR_IMAGE.jpg")!,timeoutInterval: Double.infinity)
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        request.httpBody = postData

        // Execute Post Request
        URLSession.shared.dataTask(with: request, completionHandler: { data, response, error in
            // Parse Response to String
            guard let data = data else {
                print(String(describing: error))
                return
            }

            // Convert Response String to Dictionary
            do {
                let dict = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
                if let predictedTags = dict!["predicted_classes"] as? [String]{
                    DispatchQueue.main.async {
                        for tag in predictedTags{
                            self.tags.append(tag)
                        }
                        
                        self.isLoading = false
                    }
                }
                
            } catch {
                print(error.localizedDescription)
            }

        }).resume()
        
    }
    
    // Function to upload a new work to a firestore
    func uploadWork(){
        artistId = UserDefaults.standard.string(forKey: "userId") ?? ""
        artistName = UserDefaults.standard.string(forKey: "email") ?? ""
        let uploadMetaData = StorageMetadata()
        uploadMetaData.contentType = "image/jpeg"
        let fileName = UUID().uuidString
        let ref = FirebaseManager.shared.storage.reference().child(MyKeys.imagesFolder).child(fileName)
        guard let imageData = self.image?.jpegData(compressionQuality: 0.5) else { return }
        
        let upload = ref.putData(imageData, metadata: uploadMetaData){metadata, err in
            if let err = err {
                print(err)
                return
            }
            
            ref.downloadURL{url, err in
                if let err = err {
                    print(err)
                    return
                }
                guard let url = url else{
                          print("error")
                          return
                      }
                let urlString = url.absoluteString
                let dataRef = Firestore.firestore().collection("arts").document()
                
                let dataToSave = ["artist": self.artistName,
                                  "artistID": self.artistId,
                                  "description": self.desc,
                                  "image": urlString,
                                  "galleryShow" : self.isOnGallery,
                                  "marketPlaceRevealed": self.isOnMarketplace,
                                  "name" : self.artName,
                                  "position": 0,
                                  "price" : self.price,
                                  "tags": self.tags
                ] as [String : Any]
                
                dataRef.setData(dataToSave) { err in
                    if let err = err {
                        print("error \(String(describing: err))");
                        return
                    }
                    print("Success")
                    
                }
            }
        }
        
        upload.observe(.success){(snapshot) in
            print("Upload Successful")
        }
        
        upload.observe(.failure){(snapshot) in
            print("Upload Failed")
        }

        
    }

}

// This is an extension of the class to implement all the necessary methods for the scanner view
// Has been done using the main documentation
extension UploadImagePageViewModel: VNDocumentCameraViewControllerDelegate {
    
    func documentCameraViewControllerDidCancel(_ controller: VNDocumentCameraViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
      
    func documentCameraViewController(_ controller: VNDocumentCameraViewController, didFailWithError error: Error) {
        errorMessage = error.localizedDescription
    }
      
    func documentCameraViewController(_ controller: VNDocumentCameraViewController, didFinishWith scan: VNDocumentCameraScan) {
      print("Did Finish With Scan.")
        for i in 0..<scan.pageCount {
            self.imageArray.append(scan.imageOfPage(at:i))
        }
        self.image = imageArray[0]
        controller.dismiss(animated: true, completion: nil)
    }
}
