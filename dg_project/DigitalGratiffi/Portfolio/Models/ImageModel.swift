//
//  ImagesModel.swift
//  DigitalGratiffi
//
//  Created by Vladislav Mun on 2021-12-03.
//

import Foundation
//Image Model Skeleton Based on Harvey's firebase set up

//********************IMPORTANT************************
//***It will look a little different on the firebase***


struct ImageModel: Identifiable, Decodable, Hashable{
    var id: String = UUID().uuidString
    //artist related properties
    let artistId: String
    let artistName: String
    //posted art properties
    let artName: String
    let description: String
    let artPrice: String
    let urlToImage: String
    var position: Int
    var isPinned: Bool
    var tags: [String]
    //boolean properties
    let isShownOnGallery: Bool
    let isShownOnMarketplace: Bool
}

