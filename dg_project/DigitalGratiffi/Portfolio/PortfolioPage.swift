//
//  PortfolioPage.swift
//  DigitalGratiffi
//
//  Created by Vladislav Mun on 2021-11-17.
//

import SwiftUI
import Firebase
import FirebaseStorage
import Foundation



struct MyKeys{
    static let imagesFolder = "portfolioImages"
}

class FirebaseManager: NSObject{
    let auth: Auth
    let storage: Storage
    
    static let shared = FirebaseManager()
    
    override init(){
        self.auth = Auth.auth()
        self.storage = Storage.storage()
        
        super.init()
    }
}


struct PortfolioPage: View {

    @StateObject private var viewModel = PortfolioPageViewModel()
    //@StateObject private var portfolioHomePageVM = PortfolioHomePageViewModel()
    let icons = ["rectangle.grid.2x2", "plus", "square.and.pencil"]
    @State var selected = 0;
    var body: some View{
        NavigationView{
            VStack{
                TopBar(selected: $selected)
                GeometryReader{_ in
                        if self.selected == 0{
                            PortfolioHomePage()
                        }else if self.selected == 1{
                            Text("About")
                        }
                }
            }
            .background(Color.black)
        }
    }
}

struct PortfolioPage_Previews: PreviewProvider {
    static var previews: some View {
        PortfolioPage()
    }
}
