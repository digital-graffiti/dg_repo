//
//  Record.swift
//  DigitalGratiffi
//
//  Created by Harvey Ho on 2022-10-24.
//

import Foundation

struct Record: Identifiable, Hashable, Codable{
    var id: String
    var accountId: String
    var accountName: String
    var artName: String
    var price: String
    var purchased: Bool
}
