//
//  RecordsView.swift
//  DigitalGratiffi
//
//  Created by Harvey Ho on 2022-10-24.
//

import SwiftUI
import Firebase

struct RecordsView: View {
    @State var userId: String
    @State private var refresh: Bool = false
    @ObservedObject var queryObject = QueryObject()
    
    var body: some View {
        VStack{
            Button(action: {update()}){
                Text("Refresh")
            }
            List{
                Section(header:Text("Buy")){
                    ForEach(queryObject.records, id: \.id){ r in
                        if(r.purchased == true){
                            HStack{
                                Text(r.artName)
                                Spacer()
                                Text("$"+r.price)
                            }
                        }
                    }
                }
            }
            List{
                Section(header:Text("Sell")){
                    ForEach(queryObject.records, id: \.id){ r in
                        if(r.purchased == false){
                            HStack{
                                Text(r.artName)
                                Spacer()
                                Text("$"+r.price)
                            }
                        }
                    }
                }
            }
        }
    }
    func update(){
        queryObject.getAllRecords(userId: userId)
        refresh.toggle()
    }
    init(uId: String){
        _userId = State(initialValue: uId)
        queryObject.getAllRecords(userId: userId)
    }
}

